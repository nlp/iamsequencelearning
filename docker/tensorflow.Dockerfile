# syntax=docker/dockerfile:1
FROM tensorflow/tensorflow:2.10.0-gpu

RUN apt-get update && apt-get upgrade -y 
RUN apt-get install git -y
RUN pip install --upgrade pip
RUN pip install pandas

#RUN git clone https://framagit.org/nlp/iamsequencelearning.git
#RUN pip install iamsequencelearning/.
#RUN mkdir /scripts
#RUN cp iamsequencelearning/docker/*.py /scripts
