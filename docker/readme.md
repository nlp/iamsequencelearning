# Docker deployment

Most of the commands in the host might be preceded by a `sudo` mention in order to run correctly.

## tensorflow

The Docker is build using 

```bash
docker build -f tensorflow.Dockerfile -t tensorflow/tensorflow:2.10.0 .
```

To enter the Docker:

```bash
docker run -it --rm -v $PWD/data:/data -w /tmp tensorflow/tensorflow:2.10.0 bash
```

and then do the manipulations one wants to do. e.g. `python` to start python.

`-v $PWD/data:/data` links the contents of `$PWD/data` in the host and the `/data` folder inside the container (create the folders that do not exist in either the host or the container). This is the volume strategy of Docker.

`-w /tmp` is the working directory inside the container (if `-w` links to an other folder than in `-v`, a new folder is created).

Because the Docker are run as root, any data created in the volume will have root priviledges. It means the data will not be readable in the host unless `chmod` is used.

### Run one script

To run one script in the `scripts/` folder, e.g the `hello-world.py` script, one can do

```bash
docker run -it --rm -v $PWD/:/iamsequencelearning -w /iamsequencelearning tensorflow/tensorflow:2.10.0 bash
```

to enter into the Docker, then, inside the container, one calls

```bash
pip install .
python scripts/hello-world.py
```

then `exit` from the Docker, and verify that the folder `data/` has been created in the working directory, and that it contains `hello-world` string.
