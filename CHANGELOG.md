# ChangeLog

## v0 + 

 - 0.1.1: First version
 - 0.1.2: package has __version__
 - 0.1.3: correct the metrics and binarycrossentropy output
 - 0.1.4: add sparse option as input model of tensorflow
 - 0.1.5: sparse2sparse is a sub-module
 