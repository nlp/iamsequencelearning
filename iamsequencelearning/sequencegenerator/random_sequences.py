#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Train a bag2bag neural network model, based on multi-layer perceptron or a global-attention mechanism, for different amount of noise and different predictive sequences.
"""

import numpy as np
from . import binary_random_pattern as brp


class RandomSequences(object):
    """Random sequence of X -> X pattern, plus a few of completely random samples.

    See https://pytorch.org/tutorials/beginner/basics/data_tutorial.html
    for more details.
    """

    _sequence_types = {s for s in dir(brp) if not s.startswith('__')}
    _sequence_types = _sequence_types.difference(
        ['np', 'rng', 'random', 'or_replacement'])

    def __init__(self,
                 d_input=16,
                 d_output=32,
                 size=1000,
                 noise_proportion=0.5,
                 sequence_types=()):
        super().__init__()
        self.d_input = int(d_input)
        self.d_output = int(d_output)
        self.size = int(size)
        self.noise_proportion = float(noise_proportion)
        assert 0 <= noise_proportion <= 1, 'noise_proportion must be in [0,1]'
        self.sequence_types = self._sequence_types.intersection(sequence_types)
        # not (A inter B) = A xor B = A ^ B (python bitwise operation)
        mess = "empty sequence_types and noise_proportion!=1 are strictly"
        mess += f" incompatible, received {self.sequence_types}"
        mess += f" and {self.noise_proportion}"
        assert (not len(self.sequence_types)) ^ (self.noise_proportion != 1), mess
        self._construct_data()
        return None

    def _construct_data(self):
        """Construct the datasets."""
        X, y = np.empty((0, self.d_input)), np.empty((0, self.d_output))
        # add noise
        X_, y_ = brp.random(
            size=int(self.noise_proportion * self.size),
            d_input=self.d_input, d_output=self.d_output)
        X, y = np.vstack([X, X_]), np.vstack([y, y_])
        # other types of sequences
        for seqtype in self.sequence_types:
            X_, y_ = getattr(brp, seqtype)(
                size=int((1 - self.noise_proportion)
                         * self.size) // len(self.sequence_types),
                d_input=self.d_input, d_output=self.d_output)
            X, y = np.vstack([X, X_]), np.vstack([y, y_])
        # complete with the last or random sequences
        # in case there are not enough examples
        if X.shape[0] < self.size:
            if len(self.sequence_types):
                X_, y_ = getattr(brp, seqtype)(
                    size=self.size - X.shape[0],
                    d_input=self.d_input, d_output=self.d_output)
                X, y = np.vstack([X, X_]), np.vstack([y, y_])
            else:
                X_, y_ = brp.random(
                    size=self.size - X.shape[0],
                    d_input=self.d_input, d_output=self.d_output)
                X, y = np.vstack([X, X_]), np.vstack([y, y_])
        self.X, self.y = X.astype(float), y.astype(float)
        return self.X, self.y
    
    @property
    def datas(self):
        return self.X, self.y

    def __len__(self):
        """Return the total number fo sequence."""
        return self.size

    def __getitem__(self, idx):
        """Return the idx-th element of both X and y."""
        return self.X[idx], self.y[idx]
