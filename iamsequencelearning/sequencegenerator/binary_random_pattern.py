#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generate sequence datas. Only binary variables (0 or 1) are allowed in both input or output sequences.


"""

import numpy as np
# random pattern
rng = np.random.default_rng()


def random(size=1000, d_input=16, d_output=32):
    """Completely random sequences of sizes d_input and d_output."""
    X = rng.integers(0, 2, size=(size, d_input))
    y = rng.integers(0, 2, size=(size, d_output))
    return X, y


def invert(size=1000, d_input=16, d_output=32, output_padding=False):
    """If input is 0, output is 1, and vice-versa. If d_output != d_input the output sequence is either reproduced or truncated. If d_output > d_input, there is either padding by random value 0 or 1 (once and for all padding) if output_padding is True or reproduction of the begining of the output sequence if output_padding is False (by default)."""
    X = rng.integers(0, 2, size=(size, d_input))
    x = (1 - X)[:, :d_output]
    y = x
    while y.shape[1] < d_output:
        y = np.hstack([y, np.full((size, d_output - d_input),
                                  rng.integers(0, 2))
                       if output_padding else x[:, :d_output]])
    return X[:, :d_input], y[:, :d_output]


def Z2(size=1000, d_input=16, d_output=32):
    """Alternate series of 0 and 1, only the starting position of X is random. The starting position of y is the same as y."""
    # generate the seeds
    x = np.array([rng.permutation([0, 1]) for _ in range(size)])
    X = x
    # concatenate the seeds
    while X.shape[1] < max(d_output, d_input):
        X = np.hstack([X, x])
    y = X
    return X[:, :d_input], y[:, :d_output]


def next_or(size=1000, d_input=16, d_output=32, output_padding=False):
    """Construct a random sequence. Then take the nearest neighbor sum. Finally, replace the values 2 by either 0 or 1, randomly choosen at once and for all."""
    X = rng.integers(0, 2, size=(size, d_input))
    x = np.hstack([X[:, :1], X[:, 1:] + X[:, :-1]])
    x[x == 2] = 1
    y = x
    while y.shape[1] < d_output:
        y = np.hstack([y, np.full((size, d_output - d_input),
                                  rng.integers(0, 2))
                       if output_padding else x[:, :d_output]])
    return X[:, :d_input], y[:, :d_output]


def opposite_or(size=1000, d_input=16, d_output=32, output_padding=False):
    """Construct a random sequence. Then take the sum of term i and len-i. Finally, replace the values 2 by either 0 or 1, randomly choosen at once and for all. This inevitably construct a symmetric output sequence, whatever the input sequence is."""
    X = rng.integers(0, 2, size=(size, d_input))
    x = X + X[:, ::-1]
    x[x == 2] = 1
    y = x
    while y.shape[1] < d_output:
        y = np.hstack([y, np.full((size, d_output - d_input),
                                  rng.integers(0, 2))
                       if output_padding else x[:, :d_output]])
    return X[:, :d_input], y[:, :d_output]


def next_and(size=1000, d_input=16, d_output=32, output_padding=False):
    """Construct a random sequence. Then take the nearest neighbor sum. Finally, replace the values 2 by either 0 or 1, randomly choosen at once and for all."""
    X = rng.integers(0, 2, size=(size, d_input))
    x = np.hstack([X[:, :1], X[:, 1:] * X[:, :-1]])
    y = x
    while y.shape[1] < d_output:
        y = np.hstack([y, np.full((size, d_output - d_input),
                                  rng.integers(0, 2))
                       if output_padding else x[:, :d_output]])
    return X[:, :d_input], y[:, :d_output]


def opposite_and(size=1000, d_input=16, d_output=32, output_padding=False):
    """Construct a random sequence. Then take the sum of term i and len-i. Finally, replace the values 2 by either 0 or 1, randomly choosen at once and for all. This inevitably construct a symmetric output sequence, whatever the input sequence is."""
    X = rng.integers(0, 2, size=(size, d_input))
    x = X * X[:, ::-1]
    y = x
    while y.shape[1] < d_output:
        y = np.hstack([y, np.full((size, d_output - d_input),
                                  rng.integers(0, 2))
                       if output_padding else x[:, :d_output]])
    return X[:, :d_input], y[:, :d_output]


def next_xor(size=1000, d_input=16, d_output=32, output_padding=False):
    """Construct a random sequence. Then take the nearest neighbor sum. Finally, replace the values 2 by either 0 or 1, randomly choosen at once and for all."""
    X = rng.integers(0, 2, size=(size, d_input))
    x = np.hstack([X[:, :1], X[:, 1:] + X[:, :-1]])
    x[x == 2] = 0
    y = x
    while y.shape[1] < d_output:
        y = np.hstack([y, np.full((size, d_output - d_input),
                                  rng.integers(0, 2))
                       if output_padding else x[:, :d_output]])
    return X[:, :d_input], y[:, :d_output]


def opposite_xor(size=1000, d_input=16, d_output=32, output_padding=False):
    """Construct a random sequence. Then take the sum of term i and len-i. Finally, replace the values 2 by either 0 or 1, randomly choosen at once and for all. This inevitably construct a symmetric output sequence, whatever the input sequence is."""
    X = rng.integers(0, 2, size=(size, d_input))
    x = X + X[:, ::-1]
    x[x == 2] = 0
    y = x
    while y.shape[1] < d_output:
        y = np.hstack([y, np.full((size, d_output - d_input),
                                  rng.integers(0, 2))
                       if output_padding else x[:, :d_output]])
    return X[:, :d_input], y[:, :d_output]
