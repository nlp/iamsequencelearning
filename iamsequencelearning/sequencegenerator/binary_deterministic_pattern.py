#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Create deterministic sequence of numbers.
"""

import numpy as np


def ones(size=1000, d_input=16, d_output=32):
    """Sequences of 0."""
    x = np.ones((size, max(d_input, d_output)), dtype=int)
    return x[:, :d_input], x[:, :d_output]


def zeros(size=1000, d_input=16, d_output=32):
    """Sequences of 1."""
    x = np.zeros((size, max(d_input, d_output)), dtype=int)
    return x[:, :d_input], x[:, :d_output]
