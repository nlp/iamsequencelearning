#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Generate sequence datas. Only binary variables (0 or 1) are allowed in both input or output sequences.


"""

import numpy as np
# random pattern
rng = np.random.default_rng()


def equal(size=1000, d_input=16, d_output=32, max_int=100):
    """Equal sequences of sizes d_input and d_output."""
    x = rng.integers(0, max_int + 1, size=(size, max(d_input, d_output)))
    return x[:, :d_input], x[:, :d_output]


def random(size=1000, d_input=16, d_output=32, max_int=100):
    """Completely random sequences of sizes d_input and d_output."""
    X = rng.integers(0, max_int + 1, size=(size, d_input))
    y = rng.integers(0, max_int + 1, size=(size, d_output))
    return X, y


def reversed(size=1000, d_input=16, d_output=32, max_int=100):
    """If input is 0, output is 1, and vice-versa. If d_output != d_input the output sequence is either reproduced or truncated."""
    x = rng.integers(0, max_int + 1, size=(size, max(d_input, d_output)))
    return x[:, :d_input], x[:, :d_output][:, list(range(d_output))[::-1]]


def complementary(size=1000, d_input=16, d_output=32, max_int=100):
    """Take a random sequence and its complementary, defined such that X+y=max_int."""
    # generate the seeds
    x = rng.integers(0, max_int + 1, size=(size, max(d_input, d_output)))
    return x[:, :d_input], (max_int - x)[:, :d_output]
