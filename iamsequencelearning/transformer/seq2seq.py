#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Complete seq2seq model using self-attention in an encoder-decoder architecture without recurrent network.
"""

import torch
import torch.nn as nn

class Seq2SeqModel(nn.Module):
    """Sequence-to-sequence model with elf-attention and without recurent layers."""

    def __init__(
            self,
            encoder_stack,
            decoder_stack,
            source_pad_idx,
            target_pad_idx,
            device):

        super().__init__()

        self.encoder_stack = encoder_stack
        self.decoder_stack = decoder_stack
        self.source_pad_index = source_pad_idx
        self.target_pad_index = target_pad_idx
        self.device = device
        return None

    def get_source_mask(self, source):
        """Apply the mask on the source and change dimension from (batch_size, source_len) to (batch_size, 1, 1, source_len). The AttentionHead have dimension (batch_size, n_attn_heads, Q_len, K_len)."""
        source_mask = (
            source != self.source_pad_index).unsqueeze(1).unsqueeze(2)
        return source_mask

    def get_target_mask(self, target):
        """Apply the mask on the target such as masking the newt tokens. Change also the dimensions of the target from (batch_size, source_len) to (batch_size, 1, 1, source_len)."""       
        target_pad_mask = (
            target != self.target_pad_index).unsqueeze(1).unsqueeze(2)        
        target_length = target.shape[1]        
        target_subsequent_mask = torch.tril(
            torch.ones((target_length, target_length),
                       device = self.device)).bool()
        final_target_mask = target_subsequent_mask & target_pad_mask       
        return final_target_mask

    def forward(self, source, target):
        # source = (batch_size, source_len)
        # target = (batch_size, target_len)
        source_mask = self.get_source_mask(source)
        target_mask = self.get_target_mask(target)
        #source_mask = (batch_size, 1, 1, source_len)
        #target_mask = (batch_size, 1, target_len, target_len)
        encoder_output = self.encoder_stack(source, source_mask)
        # encoder_output = (batch_size, source_len,hdim)
        decoder_output, decoder_attention = self.decoder_stack(
            target,
            encoder_output,
            source_mask,
            target_mask)
        # decoder_output = (batch_size, target_len, output_dim)
        # decoder_attention = (batch_size, num_attn_heads, target_len, source_len)
        return decoder_output, decoder_attention
