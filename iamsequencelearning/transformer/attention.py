#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Multi Head Self Attention </h3>

Self attention can completely replace recurrence.Each element in the sentence attends to other elements.Each word is a query as well as a key.  We want to score each query-key pair. In this way everything is fully contextualized.

<img src="./images/scaled_dot_product_attention.png">

<b>Example</b> <br>
$X_i$ = {$x_{i1}$,$x_{i2}$,$x_{i3}$,...$x_{in}$} <br>
Each sample will have its own query key and value vectors.These projections(Q,K,V) are just linear layers on top original word embeddings.

Now to calculate attention, take a query vector k of <b>$x_{i}$</b>, lets call it <b>$x_{ik}$</b>, and apply dot product with each of the key vectors of all <b>$x_{i}$</b> i.e apply dot product of query vector of <b>$x_{ik}$</b> with all key vectors <b>$x_{ij}$</b> for each j.

We then get attention scores after applying softmax, we do this for every word and get an attention distribution for all the words.After which we get a representation of the word which is purely context dependent.

Since all of the above is done in batches of tensors, the above calculations can be easily done using Matrix multiplications.
Below images show how to multiply inputs <b>X</b> with weight matrices <b> $W_q$, $W_v$,$W_k$ </b> to obtain the query, key and value vectors and then apply softmax on scaled dot product and multiplication with value vectors to obtain the context vector <b>Z</b>. 

In the above images Wk,Wq,Wv are nothing but the weights of the linear layers which will be used to define query,key and values.These weights are nothing but matrices for linear layers.

<b> Multiple Attention Heads <b>

In the above parts we see that the representations learnt are fully contextual in nature, and hence provide multiple subspaces of the learnt representations by each head.
<br>
As the <a href = "https://arxiv.org/abs/1706.03762">paper</a> says, in section 3.2.2 <br>
  
<i>"Instead of performing a single attention function with dmodel-dimensional keys, values and queries,
we found it beneficial to linearly project the queries, keys and values h times with different, learned
linear projections to dk, dk and dv dimensions, respectively.
    <br><br>On each of these projected versions of
queries, keys and values we then perform the attention function in parallel, yielding dv-dimensional
output values. These are concatenated and once again projected, resulting in the final values.<br>
Multi-head attention allows the model to jointly attend to information from different representation
subspaces at different positions. With a single attention head, averaging inhibits this.
"""

import torch
import torch.nn as nn

class MultiHeadSelfAttention(nn.Module):
    """
    """    
    def __init__(self,
                 d_hidden,
                 n_attention_heads,
                 dropout,
                 device):
        super().__init__()
        
        assert d_hidden % n_attention_heads == 0
        
        self.d_hidden = d_hidden
        self.n_attention_heads = n_attention_heads
        self.head_dimension = d_hidden // n_attention_heads
        
        self.W_q = nn.Linear(d_hidden, d_hidden)
        self.W_k = nn.Linear(d_hidden, d_hidden)
        self.W_v = nn.Linear(d_hidden, d_hidden)
        
        self.W_o = nn.Linear(d_hidden, d_hidden)
        
        self.dropout = nn.Dropout(dropout)
        
        self.scale = torch.sqrt(torch.FloatTensor([self.head_dimension])).to(device)
       
    
    def split_heads(self, item, batch_size):
        """The strategy is to concatenate the different attention heads. This method split them in order to apply subsequent operations on each head."""
        item = item.view(batch_size, -1, self.n_attention_heads, self.head_dimension)
        item = item.permute(0, 2, 1, 3)
        return item
    
    def forward(self, query, key, value, mask = None):
        
        batch_size = query.shape[0]
        
        Q = self.W_q(query) # Q = (bsiz, Q_len, d_hidden)
        K = self.W_k(key)  # K = (bsiz, K_len, d_hidden)
        V = self.W_v(value)  #V = (bsiz, vlen, hdim)
        
        Q = self.split_heads(Q, batch_size) # Q shape(bsiz, n_attn_heads, Q_len, head_dim)
        K = self.split_heads(K, batch_size) # K shape(bsiz, n_attn_heads, K_len, head_dim)
        V = self.split_heads(V, batch_size) # V shape(bsiz, n_attn_heads, vlen, head_dim)
             
        # permute because Q = (bsize, n_attn_heads, Q_len, head_dim)
        #             and K = (bsize, n_attn_heads, K_len, head_dim)
        # while we want to contract on head dimension
        # Q x K.T / sqrt(d_k)
        energy = torch.matmul(Q, K.permute(0,1,3,2)) / self.scale
        #energy = (bsize, n_attn_heads, Q_len, K_len)
        
        # the below masking is done so that after softmax, useless and very low values go to 0.
        if mask is not None:
            energy = energy.masked_fill(mask==0, -1e15)
        
        # softmax is applied over last dimension, across all batches, across all heads
        # softmax(Q x K.T / sqrt(d_k))
        attention = torch.softmax(energy, dim=-1)
        
        #apply attention_score x value
        #attention = (bsiz,n_attn_heads,qlen,klen)
        #V shape(bsiz,n_attn_heads,vlen,hdim)
        
        # softmax(Q x K.T / sqrt(d_k)) * V
        # equivalent to attention_scores * V
        
        attention_scored_value = torch.matmul(self.dropout(attention),V)
        #as klen = vlen, attention_scored_value(bsiz,n_attn_heads,qlen,hdim)
        
        attention_scored_value = attention_scored_value.permute(0,2,1,3).contiguous()
        
        #attention_scored_value(bsiz,qlen,n_attn_heads,hdim)
        # contiguous makes a copy of the tensor such that the order of its elements in memory
        #is the same as if it had been created from scratch with the same data.

        attention_scored_value = attention_scored_value.view(batch_size,-1,self.d_hidden)
        #attention_scored_value = (bsiz,qlen,h_dim)
        
        attention_contexts_Z = self.W_o(attention_scored_value)
        return attention_contexts_Z,attention


# First we calculate $QW^Q$, $KW^K$ and $VW^V$ with the linear layers, $W^q$, $W^k$ and $W^v$, to give us Q, K and V. Next, we split the hid_dim of the query, key and value into n_heads using .view and correctly permute them so they can be multiplied together. We then calculate the energy (the un-normalized attention) by multiplying Q and K together and scaling it by the square root of head_dim, which is calulated as hid_dim // n_heads. We then mask the energy so we do not pay attention over any elements of the sequeuence we shouldn't, then apply the softmax and dropout. We then apply the attention to the value heads, V, before combining the n_heads together. Finally, we multiply this $W^O$, represented in the code by W_o.
# 
# Note that in our implementation the lengths of the keys and values are always the same, thus when matrix multiplying the output of the softmax, attention, with V we will always have valid dimension sizes for matrix multiplication. This multiplication is carried out using torch.matmul which, when both tensors are greater than 2-dimensions, does a batched matrix multiplication over the last two dimensions of each tensor. <br>This will be a <b>[query len, key len] x [value len, head dim]</b> batched matrix multiplication over the batch size and each head which provides the<br> <b>[batch size, n heads, query len, head dim]</b> result.

# # <h3> Example 2 </h3>

# In[ ]:


bsiz = 4
q_len = 6
v_len = 6
k_len = 6


hdim = 8
nheads = 2
head_dim = hdim // nheads

Q_ = torch.rand(bsiz, q_len,hdim)
K_ = torch.rand(bsiz, k_len,hdim)
V_ = torch.rand(bsiz, v_len,hdim)

scale = torch.sqrt(torch.FloatTensor([head_dim]))


# In[ ]:


print((Q_.shape,K_.shape,V_.shape))


# In[ ]:


Q_ #[batch size, query len, hid dim]


# After applying .view() we get 4 batches each of qlen size, having nheads with each having dimension head_dim

# In[ ]:


Q_.view(bsiz,-1,nheads,head_dim).shape


# In[ ]:


for i in Q_.view(bsiz,-1,nheads,head_dim):
    print((i,i.shape))
    print('-'*50)


# In[ ]:


Q_split = Q_.view(bsiz,-1,nheads,head_dim).permute(0,2,1,3)


# In[ ]:


Q_split.shape


# In[ ]:


for splithead in Q_split:
    print((splithead,splithead.shape))
    print('-'*50)


# Q_split has shape <b>(bsize,nheads,hdim,headdim)</b>
# So there are bszie number of batches in Q_split and each batch is split into nheads , where each item is hdim x headdim.
# <br><br>
# 
# The same happens with all the three matrices -> Q,K and V.
# <br>
# 

# In[ ]:


Q_split = Q_.view(bsiz,-1,nheads,head_dim).permute(0,2,1,3)
K_split = K_.view(bsiz,-1,nheads,head_dim).permute(0,2,1,3)
V_split = V_.view(bsiz,-1,nheads,head_dim).permute(0,2,1,3)


# In[ ]:


# Q x K.T / sqrt(d_k)
energy = torch.matmul(Q_split,K_split.permute(0,1,3,2)) / scale
#energy = (bsize,n_attn_heads,qlen,klen)


# In[ ]:


energy.shape


# In[ ]:


attention = torch.softmax(energy,dim = -1)
attention.shape


# In[ ]:


#sum of attentions should be 1 across all heads
torch.sum(attention,dim=-1)
