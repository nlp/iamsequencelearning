#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The Point Wise Feed Forward Layer mentioned in section 3.3 of the https://arxiv.org/abs/1706.03762'

<i> In addition to attention sub-layers, each of the layers in our encoder and decoder contains a fully
connected feed-forward network, which is applied to each position separately and identically. This
consists of two linear transformations with a ReLU activation in between

FFN(x) = max(0, $W_1$*x + b1)*($W_2$ + $b_2$)

This is mainly used to add additional learnable parameters and give appropriate weightage to the attention outputs, while learning the other non-linearities present.
"""

import torch
import torch.nn as nn

class PointWiseFeedForwardLayer(nn.Module):
    
    def __init__(self, d_hidden, d_pointwise_feedforward, dropout):
        super().__init__()
        self.fully_connected_1 = nn.Linear(d_hidden, d_pointwise_feedforward)
        self.fully_connected_2 = nn.Linear(d_pointwise_feedforward, d_hidden)
        self.dropout = nn.Dropout(dropout)
        return None
        
    
    def forward(self, inp):
        # inp  = (batch size, sequence length, d_hidden)
        fc1_op = self.fully_connected_1(inp)
        fc1_op = torch.relu(fc1_op)
        # fc1_op = (batch size, sequence length, d_pointwise_feedforward)
        fc2_op = self.fully_connected_2(fc1_op)
        # fc2_op = (batch size, sequence length, d_hidden)
        return fc2_op
