#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
<h2> The Decoder </h2>

Decoder is used to use the encoded representations of the source <b>Z</b> and convert it to target tokens. We need to compare these predictions to find the loss and learn to improve our predictions.

<img src = './images/transformer-decoder.png'>

<img src="./images/transformer_decoding_1.gif" width="950" align="center">

The encoder outputsare transformed to a set of attention Key and Value vectors, they will be used by the decoder in its encoder decoder attention layer,which will help in decoding the appropriate positions to pay attention to while decoding.

Until a end/special token is reached, the decoder continues to decode.The output of each step is fed to the bottom decoder in the next time step, and the decoders bubble up their decoding results just like the encoders did. And just like we did with the encoder inputs, we embed and add positional encoding to those decoder inputs to indicate the position of each word.

<img src="./images/transformer_decoding_2.gif" width="950" align="center">

Each decoder block has 2 multi-head-self-attention(MHSA) layers.
<br>
These MHSA layers are used as follows:
<ul>
    <li>The first MHSA layer uses masked self attention mechanism, so that the model doesnt "<i>cheat</i>" and looks the actual word ahead of its actual decoding.
        <br><br>
    According to the paper, <br>
        <i>"We also modify the self-attention
sub-layer in the decoder stack to prevent positions from attending to subsequent positions. This
masking, combined with fact that the output embeddings are offset by one position, ensures that the
predictions for position i can depend only on the known outputs at positions less than i."</i></li>
</ul>

<ul>     <li> The second MHSA layer uses the decoder representation of query from the previous MHSA layer and encoder representations of key and value from the encoder. This is also called as <i>cross-attention</i> </li>
    
</ul>
    

<ul><li>Similar to the encoder the decoder also uses Positional Embeddings which is then combined with the scaled token embeddings by a pointwise sum.Here also the decoder can accept a sequence of max length previously given.</li>

<ul><li>The number of encoders dont need to be the same as the number of decoders.The input positional + token embedings are passed to all the stacked decoder layers. Each layer gets the encoded source sentence, along with the source and target masks. Please note that the encoder masks are used to mask attention over pad tokens and the target masks by the decoder is used to prevent the model from <i>cheating</i> by looking ahead the correct word before it even decodes it.</li>

<ul><li>The final decoder layer has a output linear layer which applies softmax over the output to predict the most probable next word.</li>

Lets implement the decoder class which implements the above, it will also use a DecoderBlock class to create a stack of decoder layers.
"""

import torch
import torch.nn as nn

# other layers
from .attention import MultiHeadSelfAttention
from .feedforward import PointWiseFeedForwardLayer


class Decoder(nn.Module):
    
    
    def __init__(self,output_dimension,hidden_dimension,
                num_decoder_layers,num_attn_heads,
                pointwise_ff_dim,dropout,
                 device,max_length = 100):
        
        super().__init__()
        
        self.device = device
        
        self.token_embeddings = nn.Embedding(output_dimension,hidden_dimension)
        self.positional_embeddings = nn.Embedding(max_length,hidden_dimension)
        
        
        self.decoder_blocks = nn.ModuleList([DecoderBlock(hidden_dimension,
                                                         num_attn_heads,
                                                         pointwise_ff_dim,
                                                         dropout,device)
                                           for _ in range(num_decoder_layers)])
        
        
        self.fully_connected_op = nn.Linear(hidden_dimension, output_dimension)
        self.dropout = nn.Dropout(dropout)
        
        self.scale = torch.sqrt(torch.FloatTensor([hidden_dimension])).to(device)
    
    
    
    def forward(self,target,encoder_src,source_mask,target_mask):
        
        batch_size = target.shape[0]
        target_len = target.shape[1]
        
        #target = (bsiz,tlen)
        #encoder_src = (bsiz,slen,hdim)
        #target_mask = (bsiz,1,tlen,tlen)
        #src_mask = (bsiz,1,1,slen)
        
        #positions same as the encoder
        #if tlen = 4 and bsiz = 3, positions.repeat(batch_size,1) will give
        #tensor([[0, 1, 2, 3, 4],
        #        [0, 1, 2, 3, 4],
        #        [0, 1, 2, 3, 4]])


        positions = torch.arange(0,target_len).unsqueeze(0)
        positions = positions.repeat(batch_size,1).to(self.device)
        #positions = (bsiz,tlen)
        
        #get the embeddings for the token and multiply with sqrt(d_model) scale
        target = self.token_embeddings(target)*self.scale
        
        #add the positional embeddings to the target
        target = target + self.positional_embeddings(positions)
        #target = (bsiz,tlen,hid_dim)
        
        
        #apply dropout
        target = self.dropout(target)
        #target = (bsiz,tlen,hid_dim)
        
        for decoder_block in self.decoder_blocks:
            target,attention = decoder_block(target,encoder_src,
                                             source_mask,target_mask)
            
        #target = (bsize,tlen,hdim)
        #attention = (bsize,num_att_heads,tlen,slen)
        
        output = self.fully_connected_op(target)
        #output = (bsize,tlen,output_dim)
        
        return output,attention



# <h3> Decoder Block </h3>

# The decoder block is almost same as the encoder block,and uses the same concepts only in a different way. As seen in the previous part and decoder image, it uses cross attention to use encoder representations of different key and value vectors and uses decoder representations for query vectors in the cross attention section.

# <img src = './images/transformer-decoder.png'>

# Like the encoder_block, the decoder_block also uses layer normalizations and residual connections as a part of its architecture,shown in the diagram above.

# The steps used in DecoderBlock are as follows : <br> <br>
# 
# <ul> 
#     <li>Step 1 : The decoderblock gets input as embeddings of size hidden_dimensions for each of the tokens in the sequence.</li><br>
#     <li>Step 2 :Apply decoder MHSA to get the key,query and value representations of the target using decoder attention.</li><br>
#     <li>Step 3 :Add a residual connection from the decoder MHSA input and add it to the decoder MHSA output, and apply decoder's Self attention Layer Normalization.</li><br>
#     <li>Step 4 :Now, pass the output of the previous layer to the encoder cross attention, using Key and Value from the encoder source and query from the target</li><br>
#     <li>Step 5 :Add a residual connection and apply encoder's cross attention layer normalization</li><br>
#     <li>Step 6 :Pass the outputs through a point wise feed forward network</li><br>
#     <li>Step 7 :Add another residual connection from the encoder cross attention outputs and apply feed forward layer normalization</li>
#     

# In[ ]:


class DecoderBlock(nn.Module):
    
    def __init__(self,hidden_dimension,num_attn_heads,
                pointwise_ff_dim,dropout,device):
        
        super().__init__()
        
        self.decoder_self_attn_layer_norm = nn.LayerNorm(hidden_dimension)
        self.encoder_cross_attn_layer_norm = nn.LayerNorm(hidden_dimension)
        self.feedForward_layer_norm = nn.LayerNorm(hidden_dimension)
        
        self.decoder_self_attention = MultiHeadSelfAttention(hidden_dimension,
                                                            num_attn_heads,
                                                            dropout,device)
        
        self.encoder_cross_attention = MultiHeadSelfAttention(hidden_dimension,
                                                            num_attn_heads,
                                                            dropout,device)
        
        self.pointWise_feedForward = PointWiseFeedForwardLayer(hidden_dimension,
                                                              pointwise_ff_dim,
                                                              dropout)
        self.dropout = nn.Dropout(dropout)

    def forward(self,target,encoder_source,source_mask,target_mask):

        #target = (bsiz,tlen,hid_dim)
        #encoder_src = (bsiz,slen,hdim)
        #target_mask = (bsiz,1,tlen,tlen)
        #src_mask = (bsiz,1,1,slen)
        
        
        #STEP2 
        #=====================================================#
        #decoder self attention
        #MHSA accepts input in (query,key,value,mask) format
        tar_self_attention_contexts,tar_self_attention_scores = self.decoder_self_attention(target,target,target,
                                                                          target_mask)
        #=====================================================#
        
        
        #STEP3
        #=====================================================#
        #dropout,residual skip connection and attn_layer norm(x+z)
        tar_self_attention_contexts = self.dropout(tar_self_attention_contexts)
        

        target = self.decoder_self_attn_layer_norm(target + tar_self_attention_contexts)
        #target = (bsiz,tlen,hdim)
        #=====================================================#

        
        
        #STEP4
        #=====================================================#
        #Encoder Cross Attention
        #MHSA accepts input in (query,key,value,mask) format
        tar_cross_attention_contexts,tar_cross_attention_scores = self.encoder_cross_attention(target,
                                                                                            encoder_source,encoder_source,
                                                                                            source_mask)
        #=====================================================#
        
        
        #STEP5
        #=====================================================#
        #encoder cross attn layer norm
        tar_cross_attention_contexts = self.dropout(tar_cross_attention_contexts)
        
        target = self.encoder_cross_attn_layer_norm(target + tar_cross_attention_contexts)
        #target = (bsiz,tlen,hdim)
        #=====================================================#
        
        
        #STEP6
        #=====================================================#
        #pointwise feedforward
        tar_cross_attention_contexts = self.pointWise_feedForward(target)
        #=====================================================#
        
        
        #STEP7
        #=====================================================#
        #dropout,residual skip connection and ff_layer norm(x+z)
        
        tar_cross_attention_contexts = self.dropout(tar_cross_attention_contexts)
        target = self.feedForward_layer_norm(target + tar_cross_attention_contexts)
        
        #=====================================================#
        
        #target = (bsiz,tlen,hdim)
        #tar_cross_attention_scores = (bsiz,nheads,tlen,slen)
        
        return target,tar_cross_attention_scores
