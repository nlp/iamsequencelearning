#!/usr/bin/env python
# coding: utf-8

# 

import torch
import torch.nn as nn

# other layers
from .attention import MultiHeadSelfAttention
from .feedforward import PointWiseFeedForwardLayer


class Encoder(nn.Module):
    """The input sequence of tokens are passed through an embedding layer, this embedding layer can be of any type, either use pretrained embeddings(Word2Vec,GLoVE,fasttext) etc. or can be learned on their own.
    <br><br>
    However since we are not using RNNs, the model has no idea about the positional sequence of input tokens, i.e the model doesn't know which word comes after what word and the relative distances between any pair of words. Hence another embedding is added to the original input embedding, which is called as positional embedding.
    <br><br>
    The positional encoding has a fixed vocab size 'x' which means the model can accept sequences which are 'x' tokens long.

    Next, the token and positional embeddings are elementwise summed together to get a vector which contains information about the token and also its position with in the sequence. However, before they are summed, the token embeddings are multiplied by a scaling factor which is $\sqrt{d_{model}}$, where $d_{model}$ is the hidden dimension size, hid_dim. This supposedly reduces variance in the embeddings and the model is difficult to train reliably without this scaling factor. Dropout is then applied to the combined embeddings.(mentioned in section 3.4 of the <a href = "https://arxiv.org/abs/1706.03762">paper</a>)
    
    
    
    The combined embeddings are then passed through $N$ encoder layers to get $Z$, which is then output and can be used by the decoder.

    WE'LL BE LEARNING THE POSITONAL ENCODINGS RATHER THAN USING STATIC PE(AS USED IN THE PAPER)

    The input sequence is the masked with a source_mask which masks the input sequence wherever there is padding. It marks the token if it is not a pad token as 1 and 0 when it is a pad token.This makes sense because the pad tokens do not serve any useful purpose apart from making the lengths similar.So the model should not apply attention over these pad tokens and hence they are masked with 0.
    """
    def __init__(
            self,
            d_input,
            d_hidden,
            n_encoder_layers,
            n_attention_heads,
            d_pointwise_feedforward,
            dropout,
            device,
            max_length = 100):
        super().__init__()
        self.device = device
        self.token_embeddings = nn.Embedding(d_input, d_hidden)
        self.positional_embeddings = nn.Embedding(max_length, d_hidden)
        self.encoder_blocks = nn.ModuleList(
            [EncoderBlock(d_hidden,
                          n_attention_heads,
                          d_pointwise_feedforward,
                          dropout,
                          device)
             for _ in range(n_encoder_layers)])
        self.dropout = nn.Dropout(dropout)
        # sqrt(d_model)
        self.scale = torch.sqrt(torch.FloatTensor([d_hidden])).to(device)
        return None
        
        
    def forward(self, source, source_mask):
        
        #src = (bsiz,slen)
        #src_mask = (bsiz,1,1,slen)
        
        batch_size = source.shape[0]
        source_length = source.shape[1]
        positions = torch.arange(0, source_length).unsqueeze(0)
        #positions = (source_length, 1)
        positions = positions.repeat(batch_size, 1).to(self.device)
        #positions = (batch_size, source_length)
        #see posmask_example1 below after this cell for clarification
        
        #add the token_embeddings to positional_encodings for letting 
        #the model know about the sequence of occurence of tokens
        
        #get the embeddings for the token and multiply with sqrt(d_model) scale
        source = self.token_embeddings(source) * self.scale
        
        #add the positional embeddings to the source
        source = source + self.positional_embeddings(positions)
        
        #apply dropout
        source = self.dropout(source)
        #source = (bsiz,slen,hdim) see the image below for tensor shape clarification
        
        for encoder_block in self.encoder_blocks:
            source = encoder_block(source,source_mask)
        
        return source


class EncoderBlock(nn.Module):
    """It is the most important part of the Encoder, it contains the main network components repeated and split across several blocks. The steps are as follows:
    <ul>
        <li>For first encoder block after passing through the embedding layers <br> it is passed through the multi head self attention layer and dropout is applied</li>
        <li>Then the input is also passed as a residual connection and through a "Layer normalization" layer</li>
        <li>It is then passed through a feed forward layer then again dropout,residual and layer normalization layer, the output of this layer is passed to the next encoder block</li>    
    </ul>
    
    The multi head self attention layer is called to find and apply attention to the sentence and it applies and returns the attention scores.
    
    <b> Layer Normalization: </b>  normalizes the values of the features, i.e. across the hidden dimension, so each feature has a mean of 0 and a standard deviation of 1. This allows neural networks with a larger number of layers, like the Transformer, to be trained easier.<br>
    See https://pytorch.org/docs/stable/generated/torch.nn.LayerNorm.html and 
    https://arxiv.org/abs/1607.06450 for more details."""
    def __init__(self,d_hidden,n_attention_heads,
                      d_pointwise_feedforward,dropout,device):
        
        super().__init__()
        
        self.attention_layer_norm = nn.LayerNorm(d_hidden)
        self.feedForward_layer_norm = nn.LayerNorm(d_hidden)
        
        self.selfAttention = MultiHeadSelfAttention(
            d_hidden,
            n_attention_heads,
            dropout,
            device)
        
        self.pointWise_feedForward = PointWiseFeedForwardLayer(
            d_hidden, d_pointwise_feedforward, dropout)
        self.dropout = nn.Dropout(dropout)
        
        
    def forward(self,source,source_mask):
        
        #source(x) = (bsiz,slen,hdim)
        #sourcemask = (bsize,1,1,slen)
        
        #self attention(z)
        #MHSA accepts input in (query,key,value,mask) format
        attention_contexts,attention_scores = self.selfAttention(source,source,source,source_mask)
        
        #dropout,residual skip connection and attn_layer norm(x+z)
        attention_contexts = self.dropout(attention_contexts)
        
        source = self.attention_layer_norm(source + attention_contexts)
        
        
        #repeat the above for feedforward layer norm
        #source = (bsiz,slen,hdim)
        
        #pointwise feedforward
        attention_contexts = self.pointWise_feedForward(source)
        
        
        #dropout,residual skip connection and ff_layer norm(x+z)
        attention_contexts = self.dropout(attention_contexts)
        source = self.feedForward_layer_norm(source + attention_contexts)
        
        #source = (bsiz,slen,hdim)
        return source





