#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
PyTorch train and test loops.

See https://pytorch.org/tutorials/beginner/basics/optimization_tutorial.html for more details.
"""
from datetime import datetime as dt
from typing import Tuple

import numpy as np

import tensorflow as tf


def predict(model, x, verbose: bool = False, sigmoid: bool = False):
    """Predict the sequence associated to the sequence x via the model."""
    y = model.predict(x, verbose=False)
    if bool(sigmoid):
        y = tf.math.sigmoid(y)  # in case the last layer has no sigmoid activation
    return tf.round(y).numpy().astype(int)


def accuracies(y_pred, y_true, meso_size: int = 3):
    """Compare y_true and y_pred on microscopic and macroscopic accuracies."""
    micro = (y_true == y_pred).sum() / np.prod(y_true.shape)
    macro = np.all(y_true == y_pred, axis=1).sum() / y_true.shape[0]
    meso = np.array([np.all(
        y_true[:, i:i + meso_size] == y_pred[:, i:i + meso_size], axis=1)
        for i in range(0, y_true.shape[1] - meso_size + 1)]
        ).sum() / y_true.shape[0] / (y_true.shape[1] - meso_size + 1)
    return micro, macro, meso


def train_loop(
        dataset: tf.data.Dataset,
        model: tf.keras.Model,
        verbose: bool = False,
        sigmoid: bool = False,
) -> float:
    """Train a model on a dataloader, using the loss_function and the optimizer."""
    accuracy_micro, accuracy_macro, accuracy_meso = 0, 0, 0
    for X, y_train in dataset:
        y_pred = model.fit(X, y_train, epochs=1, verbose=verbose)
        y_true = tf.round(y_train).numpy().astype(int)
        y_pred = predict(model, X, sigmoid=sigmoid)
        micro, macro, meso = accuracies(y_pred, y_true)
        accuracy_micro += micro
        accuracy_macro += macro
        accuracy_meso += meso
    accuracy_micro /= len(dataset)
    accuracy_macro /= len(dataset)
    accuracy_meso /= len(dataset)
    return accuracy_micro, accuracy_macro, accuracy_meso


def test_loop(
        dataset: tf.data.Dataset,
        model: tf.keras.Model,
        verbose: bool = False,
        sigmoid: bool = False,
) -> Tuple[float, float, float, float]:
    """Test a tensorflow model on a dataset.

    test_loss represents the loss of the loss_function, averaged over the number of examples.
    accuracy_micro (or micro-accuracy, or µ-accuracy) is the number of correct predictions, when all items of all sequences are supposed independent, divided by the total number of predicted items. It can be seen as an item-accuracy.
    accuracy_macro (or macro-accuracy, or M-accuracy) is the number of correct entire sequences, divided by the total number of sequences. It can be seen as a sequence-accuracy.
    accuracy_meso (or meso-accuracy, or m-accuracy) is the number of correct subsequences of a given size (10 here), divided by the total number of these subsequences. It can be seen as a subsequence-accuracy.
    """
    accuracy_micro, accuracy_macro, accuracy_meso = 0, 0, 0
    for X, y_test in dataset:
        y_pred = predict(model, X, sigmoid=sigmoid, verbose=False)
        y_true = tf.round(y_test).numpy().astype(int)
        micro, macro, meso = accuracies(y_pred, y_true)
        accuracy_micro += micro
        accuracy_macro += macro
        accuracy_meso += meso
    accuracy_micro /= len(dataset)
    accuracy_macro /= len(dataset)
    accuracy_meso /= len(dataset)
    return accuracy_micro, accuracy_macro, accuracy_meso


def traintest(
        dataset_train: tf.data.Dataset,
        dataset_test: tf.data.Dataset,
        model: tf.keras.Model,
        epochs: int,
        verbose: bool = True,
        sigmoid: bool = False,
):
    """Train and then test a model for epochs number of times."""
    training_statistics = []
    if verbose:
        t0, tt = dt.now(), dt.now()
        string = ' | '.join([
            "Epoch",
            "µ-accur.", "M-accur.", "m-accur.", "delta-t"])
        print(string)
    for epoch in range(epochs):
        train_accuracies = train_loop(
            dataset_train, model, verbose=False, sigmoid=sigmoid)
        test_accuracies = test_loop(
            dataset_test, model, verbose=False, sigmoid=sigmoid)
        training_statistics.append({
            'accuracy_micro': round(test_accuracies[0], 5),
            'accuracy_macro': round(test_accuracies[1], 5),
            'accuracy_meso': round(test_accuracies[2], 5)})
        if verbose:
            t0 = (dt.now() - t0).total_seconds()
            string = ' | '.join([
                f"{epoch+1:5}",
                f"{100*train_accuracies[0]:>7.3f}%",
                f"{100*train_accuracies[1]:>7.3f}%",
                f"{100*train_accuracies[2]:>7.3f}%",
                f"{t0:>6.2f}s"])
            print(string)
            t0 = dt.now()
    if verbose:
        tt = (dt.now() - tt).total_seconds()
        h, rest = divmod(tt, 3600)
        m, rest = divmod(rest, 60)
        print(f"Training ok in {h:n}h{m:n}m{rest:.1f}s")
    return training_statistics

def average_statistics(statistics,
                       precision: int = 6,
                       averaging_size: int = 10):
    """Extract mean and variance from the test of the models."""
    micro = [d['accuracy_micro'] for d in statistics[-averaging_size:]]
    micro_accuracy = round(np.mean(micro), precision)
    micro_accuracy_var = round(np.var(micro), precision)
    macro = [d['accuracy_macro'] for d in statistics[-averaging_size:]]
    macro_accuracy = round(np.mean(macro), precision)
    macro_accuracy_var = round(np.var(macro), precision)
    meso = [d['accuracy_meso'] for d in statistics[-averaging_size:]]
    meso_accuracy = round(np.mean(meso), precision)
    meso_accuracy_var = round(np.var(meso), precision)
    return (micro_accuracy, micro_accuracy_var,
            macro_accuracy, macro_accuracy_var,
            meso_accuracy, meso_accuracy_var)
