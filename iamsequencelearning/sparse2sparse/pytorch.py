#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A MultiLayer Perceptron models specialised in sparse matrices to sparse matrix prediction.
"""

import torch
import torch.nn as nn

class MultiLayerPerceptron(nn.Module):
    """A multi-layer perceptron class, with sigmoid functions intertwinned with linear combination of matrices."""
    def __init__(
            self,
            d_input: int = 32,
            d_hidden: int = 64,
            d_output: int = 16,
            dropout: float = 0.0,
    ):
        super().__init__()
        self.d_input = int(d_input)
        self.d_hidden = int(d_hidden)
        self.d_output = int(d_output)
        self.sequential = nn.Sequential(
            nn.Linear(in_features=self.d_input, out_features=self.d_hidden),
            nn.Sigmoid(),
            nn.Linear(in_features=self.d_hidden, out_features=self.d_hidden),
            nn.Sigmoid(),
            nn.Linear(in_features=self.d_hidden, out_features=self.d_output),
            nn.Sigmoid(),  # project in [0,1]
        )
        return None

    def forward(self, X):
        y = self.sequential(X)
        return y


class MultiLayerGlobalAttention(nn.Module):
    """Not complete."""
    def __init__(
            self,
            d_input: int = 32,
            d_embedding_input: int = 64,
            d_embedding_output: int = 64,
            d_output: int = 16,
            d_head: int = 128,
            # nhead: int = 8,
            # num_encoder_layers: int = 6,
            # num_decoder_layers: int = 6,
            # dim_feedforward: int = 2048,
            dropout: float = 0.0,
    ):
        super().__init__()
        self.d_input = int(d_input)
        self.d_embedding_input = int(d_embedding_input)
        self.d_embedding_output = int(d_embedding_output)
        self.d_output = int(d_output)
        self.d_head = int(d_head)
        self.embedding_input = nn.Linear(
            in_features=self.d_input,
            out_features=self.d_embedding_input,
            bias=True)
        self.embedding_output = nn.Linear(
            in_features=self.d_embedding_output,
            out_features=self.d_output,
            bias=True)
        self.feedforward = nn.Sequential(
            nn.Linear(in_features=d_embedding_output,
                      out_features=d_embedding_output),
            nn.ReLU(),
            nn.Linear(in_features=d_embedding_output,
                      out_features=d_embedding_output),
        )
        self.attention_query = nn.Linear(
            in_features=self.d_embedding_input,
            out_features=self.d_head,
            bias=False)
        self.attention_key = nn.Linear(
            in_features=self.d_embedding_input,
            out_features=self.d_head,
            bias=False)
        self.attention_value = nn.Linear(
            in_features=self.d_head,
            out_features=self.d_embedding_output,
            bias=False)
        self.sigmoid = nn.Sigmoid()
        self.layer_norm1 = nn.LayerNorm(self.d_embedding_output)
        self.layer_norm2 = nn.LayerNorm(self.d_embedding_output)
        return None

    def forward(self, X):
        x = self.embedding_input(X)
        query = self.attention_query(x)
        key = self.attention_key(x)
        attention = self.sigmoid(
            query * key / 
                torch.sqrt(torch.FloatTensor([self.d_head])))
        attention = self.attention_value(attention)
        attention = self.layer_norm1(attention + x)
        y = self.feedforward(x)
        attention = self.layer_norm2(attention + y)
        y = self.embedding_output(attention)
        y = self.sigmoid(y)
        return y
