#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
One-versus-rest training and testing procedures
"""

import os
import logging
from typing import Hashable
import gc

import joblib
import numpy as np
import numpy.typing as npt
import scipy.sparse as sp

import keras_core as keras

from iambagging import BagOfWords

rng = np.random.default_rng()


def balanced_onevsrest(X: BagOfWords, y: BagOfWords, target: Hashable):
    """Return X_balanced_train, y_balanced_train, X_withdrawn_train, y_withdrawn_train and/or (X_balanced_train, y_balanced_train, X_withdrawn_train, y_withdrawn_train) depending whether train and/or test datasets exist."""
    assert X.doctionarray.tolist() == y.doctionarray.tolist()
    # must copy the bags since they will be reduced by shuffle at the end
    X_balanced, X_withdrawn = X.copy(), X.copy()
    y_code = y.keep([target], axis=1)  # already generate a copy
    # since there is only one target, there is no need to do more detailed operation (as select for instance) than keep method.
    present_idx, _ = y_code.nonzero()
    # doc_idx present in X not in present_idx
    absent_idx = np.flatnonzero(np.isin(
        np.arange(X.shape[0]), present_idx, invert=True))
    if len(present_idx) < len(absent_idx):
        balanced_idx = np.concatenate(
            [present_idx,
             rng.choice(absent_idx, size=len(present_idx), replace=False)])
    else:
        balanced_idx = np.concatenate(
            [rng.choice(present_idx, size=len(absent_idx), replace=False),
             absent_idx])
    withdrawn_idx = np.arange(X_balanced.shape[0])[np.isin(
        np.arange(X.shape[0]), balanced_idx, invert=True)]
    balanced_idx = rng.permutation(balanced_idx)
    # requires BagOfWords.select and not BagOfWords.keep since the constructor of BagOfWords calls np.unique and cancel the alignment of datas among the two bags when using BagOfWords.keep
    # BagOfWords.select works inplace, so requires copy
    y_balanced, y_withdrawn = y_code.copy(), y_code.copy()
    return (
        X_balanced.select(indices=balanced_idx, axis=0),
        y_balanced.select(indices=balanced_idx, axis=0).toarray().flatten(),
        X_withdrawn.select(indices=withdrawn_idx, axis=0),
        y_withdrawn.select(indices=withdrawn_idx, axis=0).toarray().flatten())


def onevsrest(X, y):
    for code in y.vocabularray.tolist():
        yield balanced_onevsrest(X, y, code)
    return StopIteration


def class_to_onehot(y: np.asarray):
    """Neural network are used for multilabel classification, so for binary classification one has to convert the [0, 1, ...] numpy array into one-hot encoder values [[1,0], [0,1], ...]. This conversion is done here."""
    uniques = sorted(np.unique(y).tolist())
    one_hot = np.zeros((y.shape[0], len(uniques)), dtype=int)
    for i, j in enumerate(uniques):
        one_hot[y == j, i] = 1
    return one_hot, uniques


class OneVsRestClassifier():
    def __init__(self, 
                 binary_classifier,
                 binary_classifier_parameters,
                 save_folder_name: str = str()):
        self.classifier = binary_classifier
        self.binary_classifier_parameters = binary_classifier_parameters
        self.save_folder_name = str(save_folder_name)
        self.features = list()  # name of the features
        self.targets = list()  # name of the targets
        # mapping between the trained classifier and the path of its records
        # if save_folder_name is given
        # else the dictionnary of all the one-vs-rest classifiers
        self.classifiers = dict()
        return None
    def fit(self,
            X_train: BagOfWords,
            y_train: BagOfWords,
            save_folder_path: str = None) -> None:
        raise NotImplementedError
    def _append_folder_path(self, folder_path):
        """Append an extra separator in case it is not already present."""
        extra = os.sep if not folder_path.endswith(os.sep) else str()
        return folder_path + extra
    def save_one(self, classifier, folder_path: str) -> str:
        # return the folder_path to the classifier
        raise NotImplementedError
    def save(self, folder_path: str) -> str:
        """Save all the models at once."""
        folder_path = self._append_folder_path(folder_path)
        if not self.save_folder_name:
            for target in self.targets:
                self.save_one(self.classifiers[target], target, folder_path)
        np.save(folder_path + 'features.npy', np.array(self.features))
        np.save(folder_path + 'targets.npy', np.array(self.targets))
        return folder_path
    def load(self, folder_path: str):
        raise NotImplementedError        
    def predict(self, X_test: BagOfWords) -> BagOfWords:
        """Predict the entire sparse targets for a given sparse feature."""
        raise NotImplementedError
        
        
class OneVsRestClassifierSklearn(OneVsRestClassifier):
    def fit(self, X: BagOfWords, y: BagOfWords):
        self.features = X.vocabularray.tolist()
        for target in y.vocabularray:
            logging.info(f"Fit for model {self.classifier.__name__} / target {target}")
            self.targets.append(target)
            X_train, y_train, _, _ = balanced_onevsrest(X, y, target)
            clf = self.classifier(**self.binary_classifier_parameters)
            clf.fit(X_train.bag, y_train)
            if self.save_folder_name:
                self.classifiers[target] = self.save_one(
                    clf, target, self.save_folder_name)
            else:
                self.classifiers[target] = clf
        return None
    def save_one(self, clf, target, folder_path):
        folder_path = self._append_folder_path(folder_path)
        save_model_filepath = folder_path + f'{target}.joblib'
        joblib.dump(clf, save_model_filepath)
        return save_model_filepath
    def load(self, folder_path: str):
        folder_path = self._append_folder_path(folder_path)
        raise NotImplementedError
    def predict(self, X_test: BagOfWords) -> BagOfWords:
        """Can only predict just after the fit. Construct a sparse matrix that represents the outcome of all trained targets. Return this sparse matrix in the form of a BagOfWords."""
        assert X_test.vocabularray.tolist() == self.features
        # construction of the sparse prediction matrix
        data, indices, indptr = [], [], [0]
        for target in self.targets:
            if self.save_folder_name:
                clf = joblib.load(self.save_folder_name + f'{target}.joblib')
            else:
                clf = self.classifiers[target]
            y_pred = clf.predict(X_test.bag)
            raw_idx = np.flatnonzero(y_pred)
            if raw_idx.shape[0]:
                data += y_pred[raw_idx].tolist()
                indices += raw_idx.tolist()
                indptr.append(indptr[-1] + raw_idx.shape[0])
        y_pred = sp.csc_array((data, indices, indptr),
                              shape=(X_test.shape[0], len(self.targets)))
        y_pred = BagOfWords(
            bag=y_pred,
            vocabularray=self.targets,
            doctionarray=X_test.doctionarray)
        return y_pred


class BatchDensifyDataGenerator():
    """Simple batch data generator for keras. The only difference is the handling of the sparsity of X, that is done only for batch to minimize memory usage."""
    
    def __init__(self,
                 X: BagOfWords,
                 y: npt.ArrayLike = None,
                 batch_size: int = 64,
                 shuffle=True):
        if y is not None:
            assert X.shape[0] == y.shape[0]
        self.X = X
        self.y = y
        self.batch_size = batch_size
        self.shuffle = shuffle
        return None

    def on_epoch_end(self):
        if self.shuffle:
            idx = rng.permutation(range(self.X.shape[0]))
            self.X.bag = self.X.bag[idx, :]
            # useless by principle, here for consistency:
            self.X.doctionarray = self.X.doctionarray[idx]
            if self.y is not None:
                self.y = self.y[idx, :]
        return None

    def __getitem__(self, index):
        batch_slice = slice(index * self.batch_size,
                            (index + 1) * self.batch_size)
        if self.y is None:
            return self.X.bag[batch_slice, :].toarray(), None
        return self.X.bag[batch_slice, :].toarray(), self.y[batch_slice, :]

    def __len__(self):
        n, r = divmod(self.X.shape[0], self.batch_size)
        return n + int(bool(r))


class OneVsRestClassifierKeras(OneVsRestClassifier):
    epochs: int = 50
    batch_size: int = 128
    def fit(self, X: BagOfWords, y: BagOfWords):
        self.features = X.vocabularray.tolist()
        for target in y.vocabularray:
            logging.info(f"Fit for model {self.classifier.__name__} / target {target}")
            self.targets.append(target)
            X_train, y_train, _, _ = balanced_onevsrest(X, y, target)
            nclass = np.unique(y_train).shape[0]
            y_onehot = self.binary_to_onehot(y_train)
            # X_train is a BagOfWords, y_train is a 1D numpy array
            # y_onehot is a (X_train.shape[0], 2) shaped 2D numpy array
            classifier = self.classifier(
                d_input=X_train.shape[1],
                d_output=nclass,
                d_hidden=64,
                **self.binary_classifier_parameters)
            # datagen = BatchDensifyDataGenerator(
            #     X_train, y=y_onehot, batch_size=self.batch_size, shuffle=True)
            # last_epochs = []
            classifier.fit(X_train.toarray(), y_onehot,
                           batch_size=self.batch_size,
                           epochs=self.epochs,
                           verbose=False)
            # for epoch in range(self.epochs):
            #     for X, y in datagen:
            #         # X = keras.ops.convert_to_tensor(X, dtype="int8")
            #         # y = keras.ops.convert_to_tensor(y, dtype="int8")
            #         history = classifier.train_on_batch(X, y)
            #     y_pred = classifier.predict(datagen.X.toarray(), verbose=False)
            #     last_epochs.append(self.accuracy_one_epoch(
            #         datagen.y, y_pred, epoch+1))
            #     datagen.on_epoch_end()  # shuffle the data
            if self.save_folder_name:
                self.classifiers[target] = self.save_one(
                    classifier, target, self.save_folder_name)
            else:
                self.classifiers[target] = classifier
            gc.collect()
        return None
    def binary_to_onehot(self, y: np.asarray):
        """Neural network are used for multilabel classification, so for binary classification one has to convert the [0, 1, ...] numpy array into one-hot encoder values [[1,0], [0,1], ...]. This conversion is done here."""
        one_hot = np.zeros((y.shape[0], 2), dtype=int)
        for pos, value in enumerate([0, 1]):
            one_hot[y == value, pos] = 1
        return one_hot
    def onehot_to_binary(self, one_hot: np.asarray):
        """Inverse of the previous method. In case there are several 1's per row, convert to 0 value."""
        y = np.ones(one_hot.shape[0], dtype=int)
        y[one_hot[:, 0] == 1] = 0
        return y
    def convert_y_to_binary(self, y):
        """Convert the outcome of the classifier.predict to a 1D numpy array of binary values."""
        if bool('Sigmoid' not in self.classifier.__name__):
            y = keras.activations.sigmoid(y)
        y = np.round(y).astype(int)
        # y = np.round(y)
        y = self.onehot_to_binary(y)
        return y        
    def accuracy_one_epoch(self, y_true, y_pred, epoch: int):
        """y_true is a numpy array of integers, y_pred is the outcome of the neural network (."""
        # in case the last layer has no sigmoid activation
        y_pred = self.convert_y_to_binary(y_pred)
        y_true = self.onehot_to_binary(y_true)
        accuracy = np.sum(y_pred == y_true) / y_pred.shape[0]
        logging.info(f"\taccuracy for epoch n°{epoch:>3}: {100*accuracy:.4}%")
        return accuracy
    def save_one(self, classifier, target, folder_path):
        folder_path = self._append_folder_path(folder_path)
        save_model_filepath = folder_path + f'{target}.keras'
        classifier.save(save_model_filepath)
        return save_model_filepath
    def load(self, folder_path: str):
        folder_path = self._append_folder_path(folder_path)
        raise NotImplementedError       
    def predict(self, X_test: BagOfWords):
        assert X_test.vocabularray.tolist() == self.features
        # construction of the sparse prediction matrix
        data, indices, indptr = [], [], [0]
        for target in self.targets:
            logging.info(f"predict for target {target}")
            if self.save_folder_name:
                clf = keras.models.load_model(
                    self.save_folder_name + f'{target}.keras')
            else:
                clf = self.classifiers[target]
            # batch prediction
            y_pred = clf.predict(X_test.bag.toarray(), verbose=True)
            y_pred = self.convert_y_to_binary(y_pred)
            raw_idx = np.flatnonzero(y_pred)
            if raw_idx.shape[0]:
                data += y_pred[raw_idx].tolist()
                indices += raw_idx.tolist()
                indptr.append(indptr[-1] + raw_idx.shape[0])
        y_pred = sp.csc_array((data, indices, indptr),
                              shape=(X_test.shape[0], len(self.targets)))
        y_pred = BagOfWords(
            bag=y_pred,
            vocabularray=self.targets,
            doctionarray=X_test.doctionarray)
        return y_pred
