#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Scikit-learn models and their parameters. For use in several other modules.
"""

from sklearn.ensemble import (RandomForestClassifier,
                              GradientBoostingClassifier,
                              ExtraTreesClassifier)
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB, ComplementNB
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC

models = {'LR': LogisticRegression,
          'SGD': SGDClassifier,
          'DTG': DecisionTreeClassifier,
          'DTS': DecisionTreeClassifier,
          'SVC': SVC,
          'LSVC': LinearSVC,
          'RFG': RandomForestClassifier,
          'RFS': RandomForestClassifier,
          'GB': GradientBoostingClassifier,
          'ETG': ExtraTreesClassifier,
          'ETS': ExtraTreesClassifier,
          'BNB': BernoulliNB,
          'CNB': ComplementNB,
          'MNB': MultinomialNB,
          'MLP': MLPClassifier,
          }

models_names = {'LR': "Régression Logistique",
                'SGD': "Régression Logistique (via stochastic gradient descent)",
                'DTG': "Arbre de décision (séparateur de Gini)",
                'DTS': "Arbre de décision (séparateur de Shannon)",
                'SVC': "Machine à support de vecteur (SVM-Classifier)",
                'LSVC': "Machine à support de vecteur avec noyau linéaire (linear SVM-Classifier)",
                'RFG': "Foret aléatoire (séparateur de Gini)",
                'RFS': "Foret aléatoire (séparateur de Shannon)",
                'GB': "Foret aléatoire (gradient boosting)",
                'ETG': "Extra-tree classifier (séparateur de Gini)",
                'ETS': "Extra-tree classifier (séparateur de Shannon)",
                'BNB': "Réseau bayésien naif (forme Bernouili)",
                'CNB': "Réseau bayésien naif (forme complémentaire)",
                'MNB': "Réseau bayésien naif (forme multinomiale)",
                'MLP': "Perceptron multi-couche",
                }

parameters_onevsrest_classifier = {
    'SVC': dict(probability=True),
    'SGD': dict(loss='log_loss',
                max_iter=3000,
                n_jobs=-5),
    'MLP': dict(hidden_layer_sizes=(200, 200, ),
                max_iter=3000),
    'LR': dict(max_iter=3000, dual=False),
    'LSVC': dict(dual='auto'),
    'DTG': dict(criterion="gini"),
    'DTS': dict(criterion="entropy"),
    'RFG': dict(criterion="gini", n_jobs=-5),
    'RFS': dict(criterion="entropy", n_jobs=-5),
    'ETG': dict(criterion="gini", n_jobs=-5),
    'ETS': dict(criterion="entropy", n_jobs=-5),
}

parameters_chain_classifier = {
    'SVC': dict(probability=True),
    'SGD': dict(loss='log_loss',
                max_iter=3000,
                n_jobs=-5),
    'MLP': dict(hidden_layer_sizes=(200, 200, ),
                max_iter=3000,),
    'LR': dict(max_iter=3000, dual=False),
    'LSVC': dict(dual='auto'),
    'DTG': dict(criterion="gini"),
    'DTS': dict(criterion="entropy"),
    'RFG': dict(criterion="gini", n_jobs=-5),
    'RFS': dict(criterion="entropy", n_jobs=-5),
    'ETG': dict(criterion="gini", n_jobs=-5),
    'ETS': dict(criterion="entropy", n_jobs=-5),
}

parameters_multioutput_classifier = {
    'SVC': dict(probability=True),
    'SGD': dict(loss='log_loss',
                max_iter=3000,
                n_jobs=-5),
    'MLP': dict(hidden_layer_sizes=(200, 200, 200, ),
                max_iter=3000),
    'LR': dict(max_iter=3000, dual=False),
    'LSVC': dict(dual='auto'),
    'DTG': dict(criterion="gini"),
    'DTS': dict(criterion="entropy"),
    'RFG': dict(criterion="gini", n_jobs=-5, n_estimators=500),
    'RFS': dict(criterion="entropy", n_jobs=-5, n_estimators=500),
    'ETG': dict(criterion="gini", n_jobs=-5, n_estimators=500),
    'ETS': dict(criterion="entropy", n_jobs=-5, n_estimators=500),
}

strategies = {'one-vs-rest': parameters_onevsrest_classifier,
              'chain-classifier': parameters_chain_classifier,
              'multi-output': parameters_multioutput_classifier}

having_feature_importances = ['ETG', 'ETS', 'RFG', 'RFS', 'GB']
decision_trees = ['DTG', 'DTS']
naive_bayes = ['BNB', 'MNB', 'CNB']
partial_fit = ['SGD', 'MLP'] + naive_bayes


def get_classifier(model, strategy):
    """Construct the classifier associated to model (a string) and strategy (a string)."""
    try:
        classifier = models[model]
    except KeyError:
        available_models = ', '.join(models.keys())
        raise KeyError(f"model {model} is unknown, available models are {available_models}")
    try:
        parameters_strategy = strategies[strategy]
    except KeyError:
        available_strategies = ', '.join(strategies.keys())
        raise KeyError(f"strategy {model} is unknown, available strategies are {available_strategies}")
    parameters = parameters_strategy.get(model, {})
    return classifier, parameters


def train(model: str, X_train, y_train):
    """Fit the model (a string from the above dictionaries) on X_train and y_train."""
    clf, parameters = get_classifier(model, 'one-vs-rest')
    clf = clf(**parameters)
    clf.fit(X_train, y_train)
    return clf


def test(clf, X_test):
    """Test the classifier on X_test. Return y_pred."""
    return clf.predict(X_test)
