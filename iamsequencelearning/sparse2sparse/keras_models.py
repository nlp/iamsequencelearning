#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Scikit-learn models and their parameters. For use in several other modules.
"""

from iamsequencelearning.sparse2sparse.keras import (
    OneHiddenSigmoid,
    OneHiddenReLU,
    OneHiddenNone,
    AttentionSigmoid,
    Attention,
)

models = {
    'OHS': OneHiddenSigmoid,
    'OHR': OneHiddenReLU,
    'OHN': OneHiddenNone,
    'ATS': AttentionSigmoid,
    'ATT': Attention,
}

models_names = {
    'OHS': 'OneHiddenSigmoid',
    'OHR': 'OneHiddenReLU',
    'OHN': 'OneHiddenNone',
    'ATS': 'AttentionSigmoid',
    'ATT': 'Attention',
}

parameters_onevsrest_classifier = {
    'OHS': dict(),
    'OHR': dict(),
    'OHN': dict(),
    'ATS': dict(),
    'ATT': dict(),
}

parameters_chain_classifier = {
    'OHS': dict(),
    'OHR': dict(),
    'OHN': dict(),
    'ATS': dict(),
    'ATT': dict(),
}

parameters_multioutput_classifier = {
    'OHS': dict(),
    'OHR': dict(),
    'OHN': dict(),
    'ATS': dict(),
    'ATT': dict(),
}

strategies = {'one-vs-rest': parameters_onevsrest_classifier,
              'chain-classifier': parameters_chain_classifier,
              'multi-output': parameters_multioutput_classifier}


def get_classifier(model, strategy):
    """Construct the classifier associated to model (a string) and strategy (a string)."""
    try:
        classifier = models[model]
    except KeyError:
        available_models = ', '.join(models.keys())
        raise KeyError(f"model {model} is unknown, available models are {available_models}")
    try:
        parameters_strategy = strategies[strategy]
    except KeyError:
        available_strategies = ', '.join(strategies.keys())
        raise KeyError(f"strategy {model} is unknown, available strategies are {available_strategies}")
    parameters = parameters_strategy.get(model, {})
    return classifier, parameters


def train(model: str, X_train, y_train):
    """Fit the model (a string from the above dictionaries) on X_train and y_train."""
    classifier, parameters = get_classifier(model, 'one-vs-rest')
    classifier = classifier(**parameters)
    classifier.fit(X_train, y_train)
    return classifier


def test(classifier, X_test):
    """Test the classifier on X_test. Return y_pred."""
    return classifier.predict(X_test)
