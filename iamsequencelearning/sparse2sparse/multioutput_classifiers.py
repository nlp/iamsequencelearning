#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Multi-output-classifiers training and testing procedures
"""


import os
import logging

import joblib
import numpy as np

import keras_core as keras

from iambagging import BagOfWords

rng = np.random.default_rng()



class MultiOutputClassifier():
    def __init__(self, 
                 classifier,
                 parameters,
                 save_folder_name: str = str(),):
        self.classifier = classifier
        self.parameters = parameters
        self._classifier = None  # to be constructed at fit time
        self.save_folder_name = str(save_folder_name)
        self.features = list()  # name of the features
        self.targets = list()  # name of the targets
        return None
    def verify_fit(self, X: BagOfWords, y: BagOfWords):
        self.features = X.vocabularray.tolist()
        self.targets = y.vocabularray.tolist()
        assert X.doctionarray.tolist() == y.doctionarray.tolist()
        return None
    def fit(self,
            X_train: BagOfWords,
            y_train: BagOfWords,
            save_folder_path: str = None) -> None:
        # self.classifier is surcharged here
        self.verify_fit(X_train, y_train)
        self._classifier = self.classifier(**self.parameters)
        raise NotImplementedError
    def _append_folder_path(self, folder_path):
        """Append an extra separator in case it is not already present."""
        extra = os.sep if not folder_path.endswith(os.sep) else str()
        return folder_path + extra
    def save(self, folder_path: str) -> str:
        """Save all the models at once."""
        raise NotImplementedError
    def load(self, folder_path: str):
        raise NotImplementedError        
    def predict(self, X_test: BagOfWords) -> BagOfWords:
        """Predict the entire sparse targets for a given sparse feature."""
        raise NotImplementedError
        
        
class MultiOutputClassifierSklearn(MultiOutputClassifier):
    def fit(self, X: BagOfWords, y: BagOfWords):
        self.verify_fit(X, y)
        self._classifier = self.classifier(**self.parameters)
        self._classifier.fit(X.bag, y.bag.toarray())
        return None
    def save(self, folder_path):
       folder_path = self._append_folder_path(folder_path)
       save_model_filepath = folder_path + 'model.joblib'
       joblib.dump(self._classifier, save_model_filepath)
       np.save(folder_path + 'features.npy', np.array(self.features))
       np.save(folder_path + 'targets.npy', np.array(self.targets))
       return folder_path
    def load(self, folder_path: str):
        folder_path = self._append_folder_path(folder_path)
        raise NotImplementedError
    def predict(self, X_test: BagOfWords) -> BagOfWords:
        """Can only predict just after the fit. Construct a sparse matrix that represents the outcome of all trained targets. Return this sparse matrix in the form of a BagOfWords."""
        assert X_test.vocabularray.tolist() == self.features
        y_pred = self._classifier.predict(X_test.bag)
        y_pred = BagOfWords(
            bag=y_pred,
            vocabularray=self.targets,
            doctionarray=X_test.doctionarray)
        return y_pred


class BatchDensifyDataGenerator():
    """Simple batch data generator for keras. The only difference is the handling of the sparsity of X, that is done only for batch to minimize memory usage."""
    
    def __init__(self,
                 X: BagOfWords,
                 y: BagOfWords = None,  # None in case of prediction only
                 batch_size: int = 64,
                 shuffle=True):
        assert X.doctionarray.tolist() == y.doctionarray.tolist()
        self.X = X
        self.y = y
        self.batch_size = batch_size
        self.shuffle = shuffle
        return None

    def on_epoch_end(self):
        if self.shuffle:
            idx = rng.permutation(range(self.X.shape[0]))
            self.X.bag = self.X.bag[idx, :]
            self.X.doctionarray = self.X.doctionarray[idx]
            if self.y is not None:
                self.y.bag = self.y.bag[idx, :]
                self.y.doctionarray = self.y.doctionarray[idx]
                assert self.X.doctionarray.tolist() == self.y.doctionarray.tolist()
        return None

    def __getitem__(self, index):
        batch_slice = slice(index * self.batch_size,
                            (index + 1) * self.batch_size)
        if self.y is None:
            return self.X.bag[batch_slice, :].toarray()
        return self.X.bag[batch_slice, :].toarray(), self.y.bag[batch_slice, :].toarray()

    def __len__(self):
        n, r = divmod(self.X.shape[0], self.batch_size)
        return n + int(bool(r))


class MultiOutputClassifierKeras(MultiOutputClassifier):
    epochs: int = 50
    batch_size: int = 128
    def fit(self, X: BagOfWords, y: BagOfWords):
        self.verify_fit(X, y)
        logging.info(f"Fit for model {self.classifier.__name__}")
        self._classifier = self.classifier(
            d_input=X.shape[1],
            d_output=y.shape[1],
            d_hidden=128,
            **self.parameters)
        self._classifier.fit(X.bag.toarray(), y.bag.toarray(),
                             batch_size=self.batch_size,
                             epochs=self.epochs,
                             verbose=False)
        # datagen = BatchDensifyDataGenerator(
        #     X, y=y, batch_size=self.batch_size, shuffle=True)
        # last_epochs = []
        # for epoch in range(self.epochs):
        #     for X, y in datagen:
        #         history = self._classifier.train_on_batch(X, y)
        #     y_pred = self._classifier.predict(datagen.X.toarray(), verbose=False)
        #     last_epochs.append(self.accuracy_one_epoch(
        #         datagen.y.bag.toarray(), y_pred, epoch+1))
        #     datagen.on_epoch_end()
        return None
    def convert_y_to_binary(self, y):
        """Convert the outcome of the classifier.predict to a 1D numpy array of binary values."""
        # in case the last layer has no sigmoid activation
        if bool('Sigmoid' not in self.classifier.__name__):
            y = keras.activations.sigmoid(y)
        y = np.round(y).astype(int)
        return y         
    def accuracy_one_epoch(self, y_true, y_pred, epoch: int):
        """y_true is a numpy array of integers, y_pred is the outcome of the neural network (."""
        y_pred = self.convert_y_to_binary(y_pred)
        accuracy = np.sum(y_pred == y_true) / np.prod(y_pred.shape)
        logging.info(f"\taccuracy for epoch n°{epoch:>3}: {100*accuracy:.4}%")
        return accuracy
    def save(self, folder_path):
        folder_path = self._append_folder_path(folder_path)
        save_model_filepath = folder_path + 'model.keras'
        self._classifier.save(save_model_filepath)
        np.save(folder_path + 'features.npy', np.array(self.features))
        np.save(folder_path + 'targets.npy', np.array(self.targets))
        return save_model_filepath    
    def load(self, folder_path: str):
        folder_path = self._append_folder_path(folder_path)
        raise NotImplementedError       
    def predict(self, X_test: BagOfWords):
        assert X_test.vocabularray.tolist() == self.features
        y_pred = self._classifier.predict(X_test.bag.toarray(), verbose=True)
        y_pred = self.convert_y_to_binary(y_pred)
        y_pred = BagOfWords(
            bag=y_pred,
            vocabularray=self.targets,
            doctionarray=X_test.doctionarray)
        return y_pred
