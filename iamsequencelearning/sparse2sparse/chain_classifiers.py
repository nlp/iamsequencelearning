#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Chain-classifiers training and testing procedures
"""

import logging
import gc

import joblib
import numpy as np
import numpy.typing as npt
import scipy.sparse as sp

import keras_core as keras

from iambagging import BagOfWords

from .onevsrest_classifiers import (
    balanced_onevsrest, BatchDensifyDataGenerator, class_to_onehot)
from .onevsrest_classifiers import OneVsRestClassifier

rng = np.random.default_rng()

# The main differnce between a ChainClassifier and a OneVsRestClassifier is the use of features attributes
# it is a simple list for one-vs-rest strategy since all binary classifiers are trained on the same X
# while it is a list of list in the chain-classifier strategy

# Most of the implementations for subclasses below are similar to the subclasses of OneVsRestClassifier in its module
        
class ChainClassifierSklearn(OneVsRestClassifier):
    decreasing_order = True
    def fit(self, X: BagOfWords, y: BagOfWords):
        assert np.sum(np.isin(y.vocabularray, X.vocabularray)) == 0, "vocabularrays must be different"
        # pretrain: sort the y bag by increasing or decreasing number of occurence
        ycount = y.count(axis=0)
        idy = np.argsort(ycount)[::-1 if self.decreasing_order else 1]
        y.vocabularray = y.vocabularray[idy]
        y.bag = y.bag[:, idy]
        for i in range(y.shape[1]):
            # construct the features with increasing number of targets
            target = y.vocabularray[i]
            features = X.vocabularray.tolist() + y.vocabularray[:i].tolist()
            self.targets.append(target)
            self.features.append(features)
            logging.info(f"Fit for model {self.classifier.__name__} / target {target}")
            X_chain = self.concatenate(X, y, features)
            # train the one-vs-rest classifier
            X_train, y_train, _, _ = balanced_onevsrest(X_chain, y, target)
            clf = self.classifier(**self.binary_classifier_parameters)
            clf.fit(X_train.bag, y_train)
            if self.save_folder_name:
                self.classifiers[target] = self.save_one(
                    clf, target, self.save_folder_name)
            else:
                self.classifiers[target] = clf
        # pad the features array
        max_len = max(len(s) for s in self.features[-1])
        narray = np.array([['_'*max_len]*len(self.features[-1])
                           for _ in range(len(self.features))],
                          dtype=str)
        for i, features in enumerate(self.features):
            narray[i, :len(features)] = np.array(features)
        self.features = narray
        return None
    def concatenate(self, X, y, features):
        X_chain = X.copy()
        y_chain = y.keep(features, axis=1)
        assert X_chain.doctionarray.tolist() == y_chain.doctionarray.tolist()
        X_chain.bag = sp.hstack((X_chain.bag, y_chain.bag))
        X_chain.vocabularray = np.concatenate(
            (X_chain.vocabularray, y_chain.vocabularray))
        return X_chain.copy()
    def save_one(self, clf, target, folder_path):
        folder_path = self._append_folder_path(folder_path)
        save_model_filepath = folder_path + f'{target}.joblib'
        joblib.dump(clf, save_model_filepath)
        return save_model_filepath
    def load(self, folder_path: str):
        folder_path = self._append_folder_path(folder_path)
        raise NotImplementedError
    def predict(self, X_test: BagOfWords) -> BagOfWords:
        """Can only predict just after the fit. Construct a sparse matrix that represents the outcome of all trained targets. Return this sparse matrix in the form of a BagOfWords."""
        assert X_test.vocabularray.tolist() == self.features[0, :X_test.shape[1]].tolist()
        # construction of the sparse prediction matrix, column by column
        for i in range(len(self.targets)):
            target = self.targets[i]
            if self.save_folder_name:
                clf = joblib.load(self.save_folder_name + f'{target}.joblib')
            else:
                clf = self.classifiers[target]
            if i == 0:  # instanciate y_pred
                y_pred_ = clf.predict(X_test.bag)
                y_pred = sp.csc_array(y_pred_.reshape(y_pred_.shape[0],1),
                                      shape=(X_test.shape[0], 1))
            else:  # populate y_pred with a new column
                y_pred_ = clf.predict(sp.hstack([X_test.bag, y_pred]))
                y_pred_ = sp.csc_array(y_pred_.reshape(y_pred_.shape[0],1),
                                       shape=(X_test.shape[0], 1))
                y_pred = sp.hstack([y_pred, y_pred_])
        y_pred = BagOfWords(
            bag=y_pred,
            vocabularray=self.targets,
            doctionarray=X_test.doctionarray)
        return y_pred


class ChainClassifierKeras(OneVsRestClassifier):
    epochs: int = 50
    batch_size: int = 128
    decreasing_order = True
    def fit(self, X: BagOfWords, y: BagOfWords):
        # pretrain: sort the y bag by increasing or decreasing number of occurence
        ycount = y.count(axis=0)
        idy = np.argsort(ycount)[::-1 if self.decreasing_order else 1]
        y.vocabularray = y.vocabularray[idy]
        y.bag = y.bag[:, idy]
        for i in range(y.shape[1]):
            # construct the features with increasing number of targets
            target = y.vocabularray[i]
            features = X.vocabularray.tolist() + y.vocabularray[:i].tolist()
            self.targets.append(target)
            self.features.append(features)
            logging.info(f"Fit for model {self.classifier.__name__} / target {target}")
            X_chain = self.concatenate(X, y, features)
            # train the one-vs-rest classifier
            X_train, y_train, _, _ = balanced_onevsrest(X_chain, y, target)
            nclass = np.unique(y_train).shape[0]
            y_onehot = self.binary_to_onehot(y_train)
            # X_train is a BagOfWords, y_train is a 1D numpy array
            # y_onehot is a (X_train.shape[0], 2) shaped 2D numpy array
            classifier = self.classifier(
                d_input=X_train.shape[1],
                d_output=nclass,
                d_hidden=64,
                **self.binary_classifier_parameters)
            classifier.fit(X_train.bag.toarray(), y_onehot,
                           batch_size=self.batch_size,
                           epochs=self.epochs,
                           verbose=False)
            # datagen = BatchDensifyDataGenerator(
            #     X_train, y=y_onehot, batch_size=self.batch_size, shuffle=True)
            # last_epochs = []
            # for epoch in range(self.epochs):
            #     for X, y in datagen:
            #         print(X, y)
            #         history = classifier.train_on_batch(X, y)
            #     y_pred = classifier.predict(datagen.X.toarray(), verbose=False)
            #     last_epochs.append(self.accuracy_one_epoch(
            #         datagen.y, y_pred, epoch+1))
            #     datagen.on_epoch_end()
            if self.save_folder_name:
                self.classifiers[target] = self.save_one(
                    classifier, target, self.save_folder_name)
            else:
                self.classifiers[target] = classifier
        # pad the features array
        max_len = max(len(s) for s in self.features[-1])
        narray = np.array([['_'*max_len]*len(self.features[-1])
                           for _ in range(len(self.features))],
                          dtype=str)
        for i, features in enumerate(self.features):
            narray[i, :len(features)] = np.array(features)
        self.features = narray
        gc.collect()
        return None
    def concatenate(self, X, y, features):
        X_chain = X.copy()
        y_chain = y.keep(features, axis=1)
        assert X_chain.doctionarray.tolist() == y_chain.doctionarray.tolist()
        X_chain.bag = sp.hstack((X_chain.bag, y_chain.bag))
        X_chain.vocabularray = np.concatenate(
            (X_chain.vocabularray, y_chain.vocabularray))
        return X_chain.copy()    
    def binary_to_onehot(self, y: np.asarray):
        """Neural network are used for multilabel classification, so for binary classification one has to convert the [0, 1, ...] numpy array into one-hot encoder values [[1,0], [0,1], ...]. This conversion is done here."""
        one_hot = np.zeros((y.shape[0], 2), dtype=int)
        for pos, value in enumerate([0, 1]):
            one_hot[y == value, pos] = 1
        return one_hot
    def onehot_to_binary(self, one_hot: np.asarray):
        """Inverse of the previous method. In case there are several 1's per row, convert to 0 value."""
        y = np.ones(one_hot.shape[0], dtype=int)
        y[one_hot[:, 0] == 1] = 0
        return y
    def convert_y_to_binary(self, y):
        """Convert the outcome of the classifier.predict to a 1D numpy array of binary values."""
        if bool('Sigmoid' not in self.classifier.__name__):
            y = keras.activations.sigmoid(y)
        # TODO: first line works with tensorflow, second one with pytorch
        y = np.round(y).astype(int)
        # y = np.round(y)
        y = self.onehot_to_binary(y)
        return y        
    def accuracy_one_epoch(self, y_true, y_pred, epoch: int):
        """y_true is a numpy array of integers, y_pred is the outcome of the neural network (."""
        # in case the last layer has no sigmoid activation
        y_pred = self.convert_y_to_binary(y_pred)
        y_true = self.onehot_to_binary(y_true)
        accuracy = np.sum(y_pred == y_true) / y_pred.shape[0]
        logging.info(f"\taccuracy for epoch n°{epoch:>3}: {100*accuracy:.4}%")
        return accuracy
    def save_one(self, classifier, target, folder_path):
        folder_path = self._append_folder_path(folder_path)
        save_model_filepath = folder_path + f'{target}.keras'
        classifier.save(save_model_filepath)
        return save_model_filepath
    def load(self, folder_path: str):
        folder_path = self._append_folder_path(folder_path)
        raise NotImplementedError       
    def predict(self, X_test: BagOfWords):
        assert X_test.vocabularray.tolist() == self.features[0, :X_test.shape[1]].tolist()
        # construction of the sparse prediction matrix, column by column
        features = self.features[-1, X_test.shape[1]:].flatten()
        for i in range(len(self.targets)):
            target = self.targets[i]
            logging.info(f"predict for target {target}")
            if self.save_folder_name:
                clf = keras.models.load_model(
                    self.save_folder_name + f'{target}.keras')
            else:
                clf = self.classifiers[target]
            if i == 0:  # instanciate y_pred
                # batch prediction as in one-vs-rest
                y_pred_ = clf.predict(X_test.bag.toarray(), verbose=False)
                y_pred_ = self.convert_y_to_binary(y_pred_)
                y_pred = sp.csc_array(y_pred_.reshape(y_pred_.shape[0], 1),
                                      shape=(X_test.shape[0], 1))
            else:  # populate y_pred with a new column
                # populate X_text with the previous y_pred columns
                X_test_ = X_test.copy()
                X_test_.bag = sp.hstack([X_test.bag, y_pred])
                X_test_.vocabularray = np.concatenate(
                    [X_test.vocabularray, features[:i]])
                # batch prediction as in one-vs-rest, on the enlarged X_test_
                # TODO: understand why one cannot use DataGenerator here
                y_pred_ = clf.predict(X_test_.bag.toarray(), verbose=False)
                y_pred_ = self.convert_y_to_binary(y_pred_)
                y_pred_ = sp.csc_array(y_pred_.reshape(y_pred_.shape[0], 1),
                                       shape=(X_test_.shape[0], 1))
                y_pred = sp.hstack([y_pred, y_pred_])
        y_pred = BagOfWords(
            bag=y_pred,
            vocabularray=self.targets,
            doctionarray=X_test.doctionarray)
        return y_pred