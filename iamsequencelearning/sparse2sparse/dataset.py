#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Dataset is the constructor of the train and test datasets.
"""

from typing import List, Tuple

import numpy as np
import scipy.sparse as sp

from iambagging import BagOfWords

# random pattern
rng = np.random.default_rng()


class Dataset():
    """Handle dataset. Pass some train and test bags, this class deals with the alignments and filtering of datas."""

    def __init__(
            self,
            bags_train: List[Tuple[BagOfWords, BagOfWords]] = None,
            bags_test: List[Tuple[BagOfWords, BagOfWords]] = None,
            minimal_examples: int = None,
    ):
        self.minimal_examples = minimal_examples
        # combine the several bags into complete ones, that will be reduced later
        if bags_train is not None:
            self.X_train, self.y_train = self.depile_bags(bags_train)
        else:
            self.X_train, self.y_train = None, None
        if bags_test is not None:
            self.X_test, self.y_test = self.depile_bags(bags_test)
        else:
            self.X_test, self.y_test = None, None
        return None

    def depile_bags(self, bags: List[Tuple[BagOfWords, BagOfWords]]):
        """Convert a list of (X, y) bags to two bags with common documents and vocabulary."""
        def filter_common_doc(X, y):
            """Keep only common documents among the two bags."""
            common_doc = X.doctionarray[
                np.isin(X.doctionarray, y.doctionarray)]
            return X.keep(common_doc, axis=0), y.keep(common_doc, axis=0)
        try:
            X, y = filter_common_doc(*bags[0])
        except IndexError as e:
            raise IndexError("bags must be a list of tuples of BagOfWords", e)
        for X_, y_ in bags[1:]:
            X_, y_ = filter_common_doc(X_, y_)
            # update deals with the vocabularray directly
            X = X.update(X_)
            y = y.update(y_)
        assert X.shape[0] == y.shape[0]
        return X, y

    def fit(self, *args, **kwargs):
        """Do nothing."""
        return self

    def transform(self, *args, **kwargs):
        """Group examples by labels. Then withdraw examples that are not numerous enough. Then align the vocabularrays ot the different train and test bags."""
        bools = [getattr(self, 'X_train', None) is not None,
                 getattr(self, 'y_train', None) is not None]
        if all(bools):
            assert self.X_train.shape[0] == self.y_train.shape[0]
            if self.minimal_examples is not None:
                self.reduce_to_minimal_examples(int(self.minimal_examples))
            self.X_train, self.y_train = self.drop_empty_target(
                self.X_train, self.y_train)
            self.X_train = self.X_train.make_boolean()
            self.y_train = self.y_train.make_boolean()
            assert self.X_train.shape[0] == self.y_train.shape[0]
            assert self.X_train.data.tolist() == np.ones(self.X_train.nnz, dtype=int).tolist()
            assert self.y_train.data.tolist() == np.ones(self.y_train.nnz, dtype=int).tolist()
        bools = [getattr(self, 'X_test', None) is not None,
                 getattr(self, 'y_test', None) is not None]
        if all(bools):
            assert self.X_test.shape[0] == self.y_test.shape[0]
            self.X_test = self.X_test.make_boolean()
            self.y_test = self.y_test.make_boolean()
            assert self.X_test.shape[0] == self.y_test.shape[0]
            assert self.X_test.data.tolist() == np.ones(self.X_test.nnz, dtype=int).tolist()
            assert self.y_test.data.tolist() == np.ones(self.y_test.nnz, dtype=int).tolist()
        bools = [getattr(self, 'X_train', None) is not None,
                 getattr(self, 'y_train', None) is not None,
                 getattr(self, 'X_test', None) is not None,
                 getattr(self, 'y_test', None) is not None]
        if all(bools):
            self.X_test = self.align_vocabularray(self.X_train, self.X_test)
            self.y_test = self.align_vocabularray(self.y_train, self.y_test)
            assert set(self.X_train.vocabularray.tolist()) == set(self.X_test.vocabularray.tolist())
            assert set(self.y_train.vocabularray.tolist()) == set(self.y_test.vocabularray.tolist())
        return self

    def fit_transform(self, *args, **kwargs):
        """Do fit and transform in row."""
        self.fit()
        self.transform()
        return self

    def batchify(self, batch_size: int = 64, shuffle: bool = True):
        """Yield batches of the train datas."""
        batch_size = int(batch_size)
        X, y = self.X_train.copy(), self.y_train.copy()
        if bool(shuffle):
            idx = rng.permutation(self.X_train.shape[0])
            # constructor of BagOfWOrds reorder the datas using numpy.unique
            X.bag = X.bag[idx, :]
            X.doctionarray = X.doctionarray[idx]
            y.bag = y.bag[idx]
            y.doctionarray = y.doctionarray[idx]
        index = 0
        while index < X.shape[0]:
            sl = slice(index, index + batch_size)
            X_batch, y_batch = X.copy(), y.copy()
            X_batch.bag = X_batch.bag[sl, :]
            X_batch.doctionarray = X_batch.doctionarray[sl]
            y_batch.bag = y_batch.bag[sl, :]
            y_batch.doctionarray = y_batch.doctionarray[sl]
            index += batch_size
            yield X_batch.toarray(), y_batch.toarray()
        return StopIteration

    def drop_empty_target(self, X, y):
        """Drop the rows of the bags that have no target."""
        empty_docs = y.doctionarray[np.flatnonzero(y.x_count == 0)]
        return X.drop(empty_docs, axis=0), y.drop(empty_docs, axis=0)

    def reduce_to_minimal_examples(self, minimal_examples: int):
        """Reduce the vocabularray of y_train to keep only examples that are sufficient enough. If this reduction let empty documents, remove these empty documents as well."""
        minex_idx = np.flatnonzero(
            self.y_train.y_count > minimal_examples)
        tokens = self.y_train.vocabularray[minex_idx]
        # keep only the numerous examples
        self.y_train = self.y_train.keep(tokens, axis=1)
        return None

    def align_vocabularray(self, X_train, X_test):
        """Align the X_test vocabularray on the X_train vocabularray (reduce the X_test.vocabularray index but do not enlarge the X_train.vocabularray index). Helpfull to train and test machine learning models on different bags: all the features and targets must be at the same place, hence vocabularray must be the same."""
        X_test = X_test.keep(X_train.vocabularray, axis=1)
        voc_in_train_not_in_test = X_train.vocabularray[
            np.flatnonzero(np.isin(X_train.vocabularray,
                                   X_test.vocabularray, invert=True))]
        if voc_in_train_not_in_test.shape[0]:
            empty_sparse_bag = sp.csc_matrix(
                ([], ([], [])),
                shape=(X_test.shape[0], voc_in_train_not_in_test.shape[0])
            )
            bag = sp.hstack([X_test.bag, empty_sparse_bag])
            vocabularray = np.concatenate([X_test.vocabularray,
                                           voc_in_train_not_in_test])
        else:
            bag = X_test.bag
            vocabularray = X_test.vocabularray
        nbag = X_train.__class__(
            bag=bag,
            vocabularray=vocabularray,
            doctionarray=X_test.doctionarray,
            verbose=X_test.verbose)
        return nbag
