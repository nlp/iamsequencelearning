#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A MultiLayer Perceptron models specialised in sparse matrices to sparse matrix prediction.
"""

import keras_core as keras

from keras_core.layers import Activation, Add, Dense, Dropout, LayerNormalization
from keras_core import Input, Model


# TODO: construct a custom score function
# class AccuracyMicro(keras.metrics.Metric):
#     def __init__(self, name="accuracy_micro", **kwargs):
#         super().__init__(name=name, **kwargs)
#         self.count = self.add_weight(name='count', initializer='zeros')
#         self.total = self.add_weight(name='total', initializer='zeros')
#         self.accuracy = self.add_weight(name='accuracy_micro', initializer='zeros')
#     def update_state(self, y_true, y_pred, sample_weight=None):
#         count = keras.ops.cast(y_pred, keras.ops.bool) == keras.ops.cast(y_true, keras.ops.bool)
#         count = keras.ops.cast(count, self.dtype)
#         count = keras.ops.math.reduce_sum(count)
#         self.count.assign_add(count)
#         totalTrue = keras.ops.cast(y_pred, keras.ops.bool) == keras.ops.cast(True, keras.ops.bool)
#         totalTrue = keras.ops.cast(totalTrue, self.dtype)
#         totalTrue = keras.ops.math.reduce_sum(totalTrue)
#         totalFalse = keras.ops.cast(y_pred, keras.ops.bool) == keras.ops.cast(False, keras.ops.bool)
#         totalFalse = keras.ops.cast(totalFalse, self.dtype)
#         totalFalse = keras.ops.math.reduce_sum(totalFalse)
#         self.total.assign_add(totalTrue + totalFalse)
#         self.accuracy.assign_add(self.count / self.total)
#     def result(self):
#         return self.accuracy
#     def reset_state(self):
#         # The state of the metric will be reset at the start of each epoch.
#         self.accuracy = self.accuracy.assign(0.0)
#         self.count = self.count.assign(0.0)
#         self.total = self.total.assign(0.0)


metrics = [
    keras.metrics.TrueNegatives(),
    keras.metrics.TruePositives(),
    keras.metrics.FalsePositives(),
    keras.metrics.FalseNegatives(),
    keras.metrics.Accuracy(),
    keras.metrics.CategoricalAccuracy(),
    keras.metrics.CosineSimilarity(),
    # TODO: add a custom score calculation
    # AccuracyMicro(),
]

def OneHiddenSigmoid(
        d_input: int = 100,
        d_hidden: int = 128,
        d_output: int = 100,
        dropout: float = 0.0,
        sparse: bool = False):
    """Last layer is sigmoid activated, that returns probabilities instead of logits."""
    inputs = Input(shape=(int(d_input),), name='input', sparse=sparse)
    x = Dense(int(d_hidden), use_bias=False, name='embedding')(inputs)
    x = Activation(keras.activations.relu, name='relu-in')(x)
    x = Dropout(float(dropout), name='dropout')(x)
    x = Dense(int(d_hidden), name='hidden')(x)
    x = Activation(keras.activations.relu, name='relu-out')(x)
    x = Dense(int(d_output), use_bias=False, name='decodding')(x)
    outputs = Activation(keras.activations.sigmoid, name='sigmoid')(x)
    model = Model(inputs=inputs, outputs=outputs)
    loss_function = keras.losses.BinaryCrossentropy(from_logits=False)
    optimizer = keras.optimizers.Adam()
    model.compile(
        optimizer=optimizer, loss=loss_function, metrics=metrics)
    return model


def OneHiddenReLU(
        d_input: int = 100,
        d_hidden: int = 128,
        d_output: int = 100,
        dropout: float = 0.01,
        sparse: bool = False):
    """Last layer is ReLU activated, that returns probabilities instead of logits."""
    inputs = Input(shape=(int(d_input),), name='input', sparse=sparse)
    x = Dense(int(d_hidden), use_bias=False, name='embedding')(inputs)
    x = Activation(keras.activations.relu, name='relu-in')(x)
    x = Dropout(float(dropout), name='dropout')(x)
    x = Dense(int(d_hidden), name='hidden')(x)
    x = Activation(keras.activations.relu, name='relu-out')(x)
    x = Dense(int(d_output), use_bias=False, name='decodding')(x)
    outputs = Activation(keras.activations.relu, name='relu-output')(x)
    model = Model(inputs=inputs, outputs=outputs)
    loss_function = keras.losses.BinaryCrossentropy(from_logits=True)
    optimizer = keras.optimizers.Adam()
    model.compile(
        optimizer=optimizer, loss=loss_function, metrics=metrics)
    return model


def OneHiddenNone(
        d_input: int = 100,
        d_hidden: int = 128,
        d_output: int = 100,
        dropout: float = 0.01,
        sparse: bool = False):
    """Last layer is dense layer without activation, that returns probabilities instead of logits."""
    inputs = Input(shape=(int(d_input),), name='input', sparse=sparse)
    x = Dense(int(d_hidden), use_bias=False, name='embedding')(inputs)
    x = Activation(keras.activations.relu, name='relu-in')(x)
    x = Dropout(float(dropout), name='dropout')(x)
    x = Dense(int(d_hidden), name='hidden')(x)
    x = Activation(keras.activations.relu, name='relu-out')(x)
    x = Dense(int(d_output), use_bias=False, name='decodding')(x)
    outputs = x
    model = Model(inputs=inputs, outputs=outputs)
    loss_function = keras.losses.BinaryCrossentropy(from_logits=True)
    optimizer = keras.optimizers.Adam()
    model.compile(
        optimizer=optimizer, loss=loss_function, metrics=metrics)
    return model

# # this model learns nothing
# def MultiLayerPerceptron(
#         d_input: int = 32,
#         d_hidden: int = 64,
#         d_output: int = 16,
#         dropout: float = 0.0,
#         ):
#     """Last layer is not activated, hence it returns some logits."""
#     inputs = Input(shape=(int(d_input),), name='input')
#     x = Dense(int(d_hidden), activation='relu', name='hidden')(inputs)
#     x = Dropout(float(dropout), name='dropout')(x)
#     outputs = Dense(int(d_output), name='output')(x)
#     model = Model(inputs=inputs, outputs=outputs)
#     loss_function = keras.losses.BinaryCrossentropy(from_logits=True)
#     optimizer = keras.optimizers.Adam()
#     model.compile(
#         optimizer=optimizer, loss=loss_function, metrics=metrics)
#     return model


# TODO: subclass the Model class
# # this class subclassing Model can not be saved
# keras.saving.get_custom_objects().clear()
# @keras.saving.register_keras_serializable()
# class MultiLayerPerceptron(Model):

#     def __init__(
#             self,
#             d_input: int = 32,
#             d_hidden: int = 64,
#             d_output: int = 16,
#             dropout: float = 0.0):
#         self.d_input = int(d_input)
#         self.d_hidden = int(d_hidden)
#         self.d_output = int(d_output)
#         self.dropout = float(dropout)
#         inputs = Input(shape=(self.d_input,), name='input')
#         x = Dense(self.d_hidden, activation='relu', name='hidden')(inputs)
#         x = Dropout(self.dropout, name='dropout')(x)
#         outputs = Dense(self.d_output, activation='sigmoid', name='output')(x)
#         super().__init__(inputs=inputs, outputs=outputs)
#         loss_function = keras.losses.BinaryCrossentropy(from_logits=False)
#         optimizer = keras.optimizers.Adam()
#         super().compile(
#             optimizer=optimizer, loss=loss_function, metrics=metrics)
#         return None

#     def predict(self, x):
#         y = super().predict(x)
#         return keras.ops.round(y).numpy().astype(int)

#     def transform(self, x):
#         return self(x)

#     def get_config(self):
#         config = dict(
#             d_input=self.d_input,
#             d_hidden=self.d_hidden,
#             d_output=self.d_output,
#             dropout=self.dropout)
#         return config


def AttentionSigmoid(
        d_input: int = 100,
        d_hidden: int = 128,
        d_attention: int = 64,
        d_output: int = 100,
        dropout: float = 0.01,
        sparse: bool = False):
    """Last layer is sigmoid activated, that returns probabilities instead of logits."""
    inputs = Input(shape=(int(d_input),), name='input', sparse=sparse)
    x = Dense(int(d_hidden), use_bias=False, name='embedding')(inputs)
    x = Activation(keras.activations.relu, name='relu')(x)
    query = Dense(int(d_attention), use_bias=False, name='query')(x)
    key = Dense(int(d_attention), use_bias=False, name='key')(x)
    attention = keras.ops.matmul(
        query, keras.ops.transpose(key)) / keras.ops.sqrt(float(d_attention))
    attention = Activation(keras.activations.softmax, name='attention_softmax')(attention)
    value = Dense(int(d_hidden), use_bias=False, name='value')(x)
    attention = keras.ops.matmul(attention, value)
    x = Add()([x, attention])
    x = LayerNormalization()(x)
    feedforward = Dense(int(d_hidden), name='feedforward_1')(x)
    feedforward = Activation(keras.activations.relu, name='feedforward_relu')(feedforward)
    feedforward = Dropout(float(dropout), name='dropout')(feedforward)
    feedforward = Dense(int(d_hidden), name='feedforward_2')(feedforward)
    x = Add()([x, feedforward])
    x = LayerNormalization()(x)
    x = Dense(int(d_output), use_bias=False, name='decodding')(x)
    outputs = Activation(keras.activations.sigmoid, name='sigmoid')(x)
    model = Model(inputs=inputs, outputs=outputs)
    loss_function = keras.losses.BinaryCrossentropy(from_logits=False)
    optimizer = keras.optimizers.Adam()
    model.compile(
        optimizer=optimizer, loss=loss_function, metrics=metrics)
    return model


def Attention(
        d_input: int = 100,
        d_hidden: int = 128,
        d_attention: int = 64,
        d_output: int = 100,
        dropout: float = 0.01,
        sparse: bool = False):
    """Last layer is a dense layer instead of a sigmoid one."""
    inputs = Input(shape=(int(d_input),), name='input', sparse=sparse)
    x = Dense(int(d_hidden), use_bias=False, name='embedding')(inputs)
    x = Activation(keras.activations.relu, name='relu')(x)
    query = Dense(int(d_attention), use_bias=False, name='query')(x)
    key = Dense(int(d_attention), use_bias=False, name='key')(x)
    attention = keras.ops.matmul(
        query, keras.ops.transpose(key)) / keras.ops.sqrt(float(d_attention))
    attention = Activation(keras.activations.softmax, name='attention_softmax')(attention)
    value = Dense(int(d_hidden), use_bias=False, name='value')(x)
    attention = keras.ops.matmul(attention, value)
    x = Add()([x, attention])
    x = LayerNormalization()(x)
    feedforward = Dense(int(d_hidden), name='feedforward_1')(x)
    feedforward = Activation(keras.activations.relu, name='feedforward_relu')(feedforward)
    feedforward = Dropout(float(dropout), name='dropout')(feedforward)
    feedforward = Dense(int(d_hidden), name='feedforward_2')(feedforward)
    x = Add()([x, feedforward])
    x = LayerNormalization()(x)
    x = Dense(int(d_output), use_bias=False, name='decodding')(x)
    outputs = x
    model = Model(inputs=inputs, outputs=outputs)
    loss_function = keras.losses.BinaryCrossentropy(from_logits=True)
    optimizer = keras.optimizers.Adam()
    model.compile(
        optimizer=optimizer, loss=loss_function, metrics=metrics)
    return model
