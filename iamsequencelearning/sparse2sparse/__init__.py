#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import logging

from . import keras_models, sklearn_models
from . import onevsrest_classifiers
from . import chain_classifiers
from . import multioutput_classifiers
from .dataset import Dataset

modelset = set()
for module_models in [keras_models, sklearn_models]:
    modelset.update(module_models.models.keys())

strategy2module = {
    'one-vs-rest': onevsrest_classifiers,
    'chain-classifier': chain_classifiers,
    'multi-output': multioutput_classifiers,
}

strategieset = set(strategy2module.keys())

strategy2classifier = {s: {} for s in strategieset}
for strategy, module in strategy2module.items():
    for attr in dir(module):
        if attr.endswith('Keras'):
            strategy2classifier[strategy]['keras'] = getattr(module, attr)
        elif attr.endswith('Sklearn'):
            strategy2classifier[strategy]['sklearn'] = getattr(module, attr)


def resolve_model_strategy(model, strategy):
    """From a model name and a strategy name (in given nomenclature), return a iamsequencelearning.Classifier"""
    if model not in modelset:
        raise KeyError(f"model {model} is unknown, available ones are {modelset}")
    if strategy not in strategieset:
        raise KeyError(f"strategy {strategy} is unknown, available ones are {strategieset}")
    if model in sklearn_models.models:
        classifier, parameters = sklearn_models.get_classifier(model, strategy)
        Classifier = strategy2classifier[strategy]['sklearn'](classifier, parameters)
    elif model in keras_models.models:
        classifier, parameters = keras_models.get_classifier(model, strategy)
        Classifier = strategy2classifier[strategy]['keras'](classifier, parameters)
    return Classifier

def create_folder(folder_path):
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    return folder_path

def traintest(dataset: Dataset,
              models,
              strategies,
              trainings_folder_name: str = None,
              testings_folder_name: str = None,
              parameters: dict = dict()):
    """Train and test (or fit and predict in local parlance) the models using the strategies on the dataset.
    
    trainings_folder_name will receive the trained models
    testings_folder_name will receive the test results
    """

    parameters_global = {**parameters}

    for strategy in strategies:

        models_folder_name = create_folder(
            trainings_folder_name + strategy + os.sep)
        tests_folder_name = create_folder(
            testings_folder_name + strategy + os.sep)
        
        logging.info(f"Start strategy {strategy}")
        
        for model in models:
        
            logging.info(f"Start model {model}")
        
            model_folder_name = create_folder(
                models_folder_name + model + os.sep)
            test_folder_name = create_folder(
                tests_folder_name + model + os.sep)
        
            CLF = resolve_model_strategy(model, strategy)
            CLF.save_folder_name = model_folder_name
            CLF.fit(dataset.X_train, dataset.y_train)
            dataset.y_train.save(test_folder_name + 'train')
            CLF.save(model_folder_name)
            logging.info("Training ok... start testing ...")
            y_pred = CLF.predict(dataset.X_test)
            y_pred.save(test_folder_name + 'pred')
            logging.info("... testing ok ... testing again on the train datas...")
            y_easy = CLF.predict(dataset.X_train)
            y_easy.save(test_folder_name + 'easy')
            dataset.y_test.save(test_folder_name + 'true')
            logging.info(f'saved test results in {test_folder_name}')

    logging.info("End of script")

    return None
