#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Sequence transformation.
"""

import numpy as np
import scipy.sparse as sp
# random pattern
rng = np.random.default_rng()


def shuffle(X, y):
    """Shuffle X and y arrays along they first axis."""
    assert X.shape[0] == y.shape[0], "X and y must have same length."
    idx = rng.permutation(X.shape[0])
    return X[idx], y[idx]


def batchify(X, y, batch_size=16):
    """Separate the X and y arrays of sizes (L, ...), (L, ...) (other dimensions can be different into two arrays of X and y arrays of sizes (L/batch_size, batch_size, ...) and (L/batch_size, batch_size, ...). The un-complete last part is lost."""
    assert X.shape[0] == y.shape[0], "X and y must have same length."
    nb_batch, rest = divmod(X.shape[0], batch_size)
    X_shape, y_shape = X.shape, y.shape
    if sp.issparse(X):
        X_ = [X[i * batch_size: i * batch_size + batch_size]
              for i in range(nb_batch)]
        if nb_batch * batch_size < X.shape[0]:
            X_.append(X[nb_batch * batch_size:])
    else:        
        X_ = X[:nb_batch * batch_size]
        X_ = X_.reshape(nb_batch, batch_size, *X_shape[1:])
    if sp.issparse(y):
        y_ = [y[i * batch_size: i * batch_size + batch_size]
              for i in range(batch_size)]
        if nb_batch * batch_size < y.shape[0]:
            y_.append(y[nb_batch * batch_size:])
    else:
        y_ = y[:nb_batch * batch_size]
        y_ = y_.reshape(nb_batch, batch_size, *y_shape[1:])
    return X_, y_
