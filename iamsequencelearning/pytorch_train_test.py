#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
PyTorch train and test loops.

See https://pytorch.org/tutorials/beginner/basics/optimization_tutorial.html for more details.
"""
from datetime import datetime as dt
from typing import Tuple

import torch
import numpy as np


def train_loop(
        dataloader: torch.utils.data.DataLoader,
        model: torch.nn.Module,
        loss_function: torch.nn.Module,
        optimizer: torch.optim.Optimizer,
) -> float:
    """Train a model on a dataloader, using the loss_function and the optimizer."""
    # Set the model to training mode - important for batch normalization and dropout layers
    # Unnecessary in this situation but added for best practices
    model.train()
    loss_average = 0
    for X, y_train in dataloader:
        # Compute prediction and loss
        y_pred = model(X)
        loss = loss_function(y_pred, y_train)
        loss_average += loss.item()
        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    return loss_average / len(dataloader)


def test_loop(
        dataloader: torch.utils.data.DataLoader,
        model: torch.nn.Module,
        loss_function: torch.nn.Module,
) -> Tuple[float, float, float, float]:
    """Test a model on a dataloader.

    test_loss represents the loss of the loss_function, averaged over the number of examples.
    accuracy_micro (or micro-accuracy, or µ-accuracy) is the number of correct predictions, when all items of all sequences are supposed independent, divided by the total number of predicted items. It can be seen as an item-accuracy.
    accuracy_macro (or macro-accuracy, or M-accuracy) is the number of correct entire sequences, divided by the total number of sequences. It can be seen as a sequence-accuracy.
    accuracy_meso (or meso-accuracy, or m-accuracy) is the number of correct subsequences of a given size (10 here), divided by the total number of these subsequences. It can be seen as a subsequence-accuracy.
    """
    # Set the model to evaluation mode - important for batch normalization and dropout layers
    # Unnecessary in this situation but added for best practices
    model.eval()
    meso_size = 10
    test_loss, accuracy_micro, accuracy_macro, accuracy_meso = 0, 0, 0, 0

    # Evaluating the model with torch.no_grad() ensures that no gradients are computed during test mode
    # also serves to reduce unnecessary gradient computations and memory usage for tensors with requires_grad=True
    with torch.no_grad():
        for X, y in dataloader:
            pred = model(X)
            test_loss += loss_function(pred, y).item()
            y_pred = pred.round().type(torch.int).numpy()
            y_true = y.round().type(torch.int).numpy()
            accuracy_micro += (y_true == y_pred).sum() / np.prod(y_true.shape)
            accuracy_macro += np.all(y_true == y_pred, axis=1).sum() / y_true.shape[0]
            accuracy_meso += np.array([np.all(
                y_true[:, i:i+meso_size] == y_pred[:, i:i+meso_size], axis=1)
                for i in range(0, y_true.shape[1] - meso_size + 1)]
                ).sum() / y_true.shape[0] / (y_true.shape[1] - meso_size + 1)

    test_loss /= len(dataloader)
    accuracy_micro /= len(dataloader)
    accuracy_macro /= len(dataloader)
    accuracy_meso /= len(dataloader)
    return test_loss, accuracy_micro, accuracy_macro, accuracy_meso


def traintest(
        epochs: int,
        dataloader_train: torch.utils.data.DataLoader,
        dataloader_test: torch.utils.data.DataLoader,
        model: torch.nn.Module,
        loss_function: torch.nn.Module,
        optimizer: torch.optim.Optimizer,
        verbose: bool = True,
):
    """Train and then test a model for epochs number of times."""
    if verbose:
        string = ' | '.join([
            "Epoch", "Train loss", " Test loss",
            "µ-accur.", "M-accur.", "m-accur.", "t elapsed"])
        print(string)
        t0, tt = dt.now(), dt.now()
    training_statistics = []
    for epoch in range(epochs):
        train_loss_avg = train_loop(
            dataloader_train, model, loss_function, optimizer)
        test_loss, accuracy_micro, accuracy_macro, accuracy_meso = test_loop(
            dataloader_test, model, loss_function)
        training_statistics.append(
            {'epoch': epoch,
             'train_loss': round(train_loss_avg, 5),
             'test_loss': round(test_loss, 5),
             'accuracy_micro': round(accuracy_micro, 5),
             'accuracy_macro': round(accuracy_macro, 5),
             'accuracy_meso': round(accuracy_meso, 5)})
        if verbose:
            t0 = (dt.now() - t0).total_seconds()
            string = ' | '.join([
                f"{epoch+1:5}",
                f"{train_loss_avg:>10.5f}",
                f"{test_loss:>10.5f}",
                f"{100*accuracy_micro:>7.3f}%",
                f"{100*accuracy_macro:>7.3f}%",
                f"{100*accuracy_meso:>7.3f}%",
                f"{t0:>8.2f}s"])
            print(string)
            t0 = dt.now()
    if verbose:
        tt = (dt.now() - tt).total_seconds()
        h, rest = divmod(tt, 3600)
        m, rest = divmod(rest, 60)
        print(f"Training ok in {h:n}h{m:n}m{rest:.1f}s")
    return training_statistics


def average_statistics(statistics,
                       precision: int = 6,
                       averaging_size: int = 10):
    """Extract mean and variance from the test of the models."""
    micro = [d['accuracy_micro'] for d in statistics[-averaging_size:]]
    micro_accuracy = round(np.mean(micro), precision)
    micro_accuracy_var = round(np.var(micro), precision)
    macro = [d['accuracy_macro'] for d in statistics[-averaging_size:]]
    macro_accuracy = round(np.mean(macro), precision)
    macro_accuracy_var = round(np.var(macro), precision)
    meso = [d['accuracy_meso'] for d in statistics[-averaging_size:]]
    meso_accuracy = round(np.mean(meso), precision)
    meso_accuracy_var = round(np.var(meso), precision)
    return (micro_accuracy, micro_accuracy_var,
            macro_accuracy, macro_accuracy_var,
            meso_accuracy, meso_accuracy_var)
