#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Train a bag2bag neural network model, to familiarize with the submodels strategies.
"""
import os
os.environ["KERAS_BACKEND"] = "torch"

import logging

import numpy as np
from iambagging import BagOfWords

from iamsequencelearning.sequencegenerator.random_sequences import RandomSequences
import iamsequencelearning.sparse2sparse as s2s

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s|%(levelname)s|%(message)s',
    datefmt="%Y-%m-%d|%H:%M:%S",
)

available_sequence_types = RandomSequences._sequence_types

# make easy task
d_input = 25
d_output = 12
size_train = 250

# =============================================================================
# Train several one-vs-rest classifiers
# =============================================================================

dataset_train = RandomSequences(
    d_input=d_input,
    d_output=d_output,
    size=size_train,
    noise_proportion=0.1,
    sequence_types=['invert'])
X, y = dataset_train.datas
X = BagOfWords(
    bag=X,
    vocabularray=[str(i+1) for i in range(X.shape[1])],
    doctionarray=[str(i+i) for i in range(X.shape[0])])
y = BagOfWords(
    bag=y,
    vocabularray=[str(i+1) for i in range(y.shape[1])],
    doctionarray=[str(i+i) for i in range(y.shape[0])])

accuracies = []
clf = s2s.resolve_model_strategy('SGD', 'one-vs-rest')
clf.fit(X, y)
y_pred = clf.predict(X)
accuracies.append(np.sum(y_pred.toarray() == y.toarray()) / np.prod(y.shape))
folder_path = 'data/SGD'
s2s.create_folder(folder_path)
clf.save(folder_path)
clf = s2s.resolve_model_strategy('OHN', 'one-vs-rest')
clf.epochs = 2
clf.batch_size = 32
clf.fit(X, y)
y_pred = clf.predict(X)
accuracies.append(np.sum(y_pred.toarray() == y.toarray()) / np.prod(y.shape))
folder_path = 'data/OHN'
s2s.create_folder(folder_path)
clf.save(folder_path)


# =============================================================================
# Train several MultiOutput classifiers
# =============================================================================

dataset_train = RandomSequences(
    d_input=d_input,
    d_output=d_output,
    size=size_train,
    noise_proportion=0.1,
    sequence_types=['invert'])
X, y = dataset_train.datas
X = BagOfWords(
    bag=X,
    vocabularray=[str(i+1) for i in range(X.shape[1])],
    doctionarray=[str(i+i) for i in range(X.shape[0])])
y = BagOfWords(
    bag=y,
    vocabularray=[str(i+1) for i in range(y.shape[1])],
    doctionarray=[str(i+i) for i in range(y.shape[0])])

accuracies = []
clf = s2s.resolve_model_strategy('MLP', 'multi-output')
clf.fit(X, y)
y_pred = clf.predict(X)
accuracies.append(np.sum(y_pred.toarray() == y.toarray()) / np.prod(y.shape))
folder_path = 'data/MLP'
s2s.create_folder(folder_path)
clf.save(folder_path)
clf = s2s.resolve_model_strategy('OHS', 'multi-output')
clf.epochs = 2
clf.batch_size = 32
clf.fit(X, y)
y_pred = clf.predict(X)
# automatically sort the different doctionarray and vocabularray indexes at construction
y_pred = BagOfWords(y_pred)
y = BagOfWords(y)
assert y.doctionarray.tolist() == y_pred.doctionarray.tolist()
assert y.vocabularray.tolist() == y_pred.vocabularray.tolist()
accuracies.append(np.sum(y_pred.toarray() == y.toarray()) / np.prod(y.shape))
folder_path = 'data/OHS'
s2s.create_folder(folder_path)
clf.save(folder_path)


# =============================================================================
# Train several Chain-Classifier classifiers
# =============================================================================

dataset_train = RandomSequences(
    d_input=d_input,
    d_output=d_output,
    size=size_train,
    noise_proportion=0.1,
    sequence_types=['invert'])
X, y = dataset_train.datas
X = BagOfWords(
    bag=X,
    vocabularray=[str(i+1) for i in range(X.shape[1])],
    doctionarray=[str(i+i) for i in range(X.shape[0])])
y = BagOfWords(
    bag=y,
    vocabularray=['y_' + str(i+1) for i in range(y.shape[1])],
    doctionarray=[str(i+i) for i in range(y.shape[0])])

accuracies = []
clf = s2s.resolve_model_strategy('SGD', 'chain-classifier')
clf.fit(X, y)
y_pred = clf.predict(X)
y_pred = BagOfWords(y_pred)
y = BagOfWords(y)
assert y.doctionarray.tolist() == y_pred.doctionarray.tolist()
assert y.vocabularray.tolist() == y_pred.vocabularray.tolist()
accuracies.append(np.sum(y_pred.toarray() == y.toarray()) / np.prod(y.shape))
folder_path = 'data/SGD2'
s2s.create_folder(folder_path)
clf.save(folder_path)

clf = s2s.resolve_model_strategy('OHR', 'chain-classifier')
clf.epochs = 18
clf.batch_size = 32
clf.fit(X, y)
y_pred = clf.predict(X)
y_pred = BagOfWords(y_pred)
y = BagOfWords(y)
assert y.doctionarray.tolist() == y_pred.doctionarray.tolist()
assert y.vocabularray.tolist() == y_pred.vocabularray.tolist()
accuracies.append(np.sum(y_pred.toarray() == y.toarray()) / np.prod(y.shape))
folder_path = 'data/OHR'
s2s.create_folder(folder_path)
clf.save(folder_path)

# =============================================================================
# Train the ChainClassifier and the MultiOutputClassifier classes of sklearn
# =============================================================================

import numpy as np
from sklearn.datasets import make_multilabel_classification
from sklearn.multioutput import MultiOutputClassifier, ClassifierChain
from sklearn.linear_model import LogisticRegression, SGDClassifier


dataset_train = RandomSequences(
    d_input=d_input,
    d_output=d_output,
    size=size_train,
    noise_proportion=0.1,
    sequence_types=['invert'])
X, y = dataset_train.datas
X = BagOfWords(
    bag=X,
    vocabularray=[str(i+1) for i in range(X.shape[1])],
    doctionarray=[str(i+i) for i in range(X.shape[0])])
y = BagOfWords(
    bag=y,
    vocabularray=['y_' + str(i+1) for i in range(y.shape[1])],
    doctionarray=[str(i+i) for i in range(y.shape[0])])

accuracies = []

clf = s2s.resolve_model_strategy('LR', 'one-vs-rest')
clf.fit(X, y)
y_pred = clf.predict(X)
y_pred = BagOfWords(y_pred)
y = BagOfWords(y)
assert y.doctionarray.tolist() == y_pred.doctionarray.tolist()
assert y.vocabularray.tolist() == y_pred.vocabularray.tolist()
accuracies.append(np.sum(y_pred.toarray() == y.toarray()) / np.prod(y.shape))
accuracies.append(np.sum([y_pred.toarray()[i, :] == y.toarray()[i, :]
                          for i in range(y.shape[0])]) / y.shape[0])
clf = MultiOutputClassifier(LogisticRegression())
clf.fit(X.bag, y.toarray())
y_pred = clf.predict(X.bag)
accuracies.append(np.sum(y_pred == y.toarray()) / np.prod(y.shape))
accuracies.append(np.sum([y_pred[i, :] == y.toarray()[i, :]
                          for i in range(y.shape[0])]) / y.shape[0])

clf = s2s.resolve_model_strategy('LR', 'chain-classifier')
clf.fit(X, y)
y_pred = clf.predict(X)
y_pred = BagOfWords(y_pred)
y = BagOfWords(y)
assert y.doctionarray.tolist() == y_pred.doctionarray.tolist()
assert y.vocabularray.tolist() == y_pred.vocabularray.tolist()
accuracies.append(np.sum(y_pred.toarray() == y.toarray()) / np.prod(y.shape))
accuracies.append(np.sum([y_pred.toarray()[i, :] == y.toarray()[i, :]
                          for i in range(y.shape[0])]) / y.shape[0])
clf = ClassifierChain(LogisticRegression(), order=np.arange(y.shape[1]))
clf.fit(X.bag, y.toarray())
y_pred = clf.predict(X.bag)
accuracies.append(np.sum(y_pred == y.toarray()) / np.prod(y.shape))
accuracies.append(np.sum([y_pred[i, :] == y.toarray()[i, :]
                          for i in range(y.shape[0])]) / y.shape[0])

lclf = SGDClassifier(loss='log_loss', max_iter=3000, n_jobs=-5)
clf = s2s.resolve_model_strategy('SGD', 'one-vs-rest')
clf.fit(X, y)
y_pred = clf.predict(X)
y_pred = BagOfWords(y_pred)
y = BagOfWords(y)
assert y.doctionarray.tolist() == y_pred.doctionarray.tolist()
assert y.vocabularray.tolist() == y_pred.vocabularray.tolist()
accuracies.append(np.sum(y_pred.toarray() == y.toarray()) / np.prod(y.shape))
accuracies.append(np.sum([y_pred.toarray()[i, :] == y.toarray()[i, :]
                          for i in range(y.shape[0])]) / y.shape[0])
clf = MultiOutputClassifier(lclf)
clf.fit(X.bag, y.toarray())
y_pred = clf.predict(X.bag)
accuracies.append(np.sum(y_pred == y.toarray()) / np.prod(y.shape))
accuracies.append(np.sum([y_pred[i, :] == y.toarray()[i, :]
                          for i in range(y.shape[0])]) / y.shape[0])

clf = s2s.resolve_model_strategy('SGD', 'chain-classifier')
clf.fit(X, y)
y_pred = clf.predict(X)
y_pred = BagOfWords(y_pred)
y = BagOfWords(y)
assert y.doctionarray.tolist() == y_pred.doctionarray.tolist()
assert y.vocabularray.tolist() == y_pred.vocabularray.tolist()
accuracies.append(np.sum(y_pred.toarray() == y.toarray()) / np.prod(y.shape))
accuracies.append(np.sum([y_pred.toarray()[i, :] == y.toarray()[i, :]
                          for i in range(y.shape[0])]) / y.shape[0])
clf = ClassifierChain(lclf, order=np.arange(y.shape[1]))
clf.fit(X.bag, y.toarray())
y_pred = clf.predict(X.bag)
accuracies.append(np.sum(y_pred == y.toarray()) / np.prod(y.shape))
accuracies.append(np.sum([y_pred[i, :] == y.toarray()[i, :]
                          for i in range(y.shape[0])]) / y.shape[0])