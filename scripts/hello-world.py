#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Just write a hello-world in data/hello-world.txt file
"""
import os

foldername = 'data/'
if not os.path.exists(foldername):
    os.makedirs(foldername)

string = "hello world"

with open(foldername + "test.txt", "w") as f:
    f.write(string)
