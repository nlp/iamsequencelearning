#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
os.environ["KERAS_BACKEND"] = "torch"

import numpy as np
import keras_core as keras
import tensorflow as tf
import torch

print("Keras version: ", keras.__version__)
print("numpy version: ", np.__version__)
print("tensorflow version: ", tf.__version__)
print("torch version: ", torch.__version__)

d_input = 12
d_output = 5

inputs = keras.Input(shape=(int(d_input),))
outputs = keras.layers.Dense(
    int(d_output), use_bias=False, activation='sigmoid')(inputs)
model = keras.Model(inputs=inputs, outputs=outputs)
loss_function = keras.losses.BinaryCrossentropy(from_logits=False)
optimizer = keras.optimizers.Adam()
metrics = [keras.metrics.Accuracy()]
model.compile(optimizer=optimizer, loss=loss_function, metrics=metrics)

batch_size = 24

X = np.random.randint(0, 2, size=(2*batch_size, d_input))
y = np.random.randint(0, 2, size=(2*batch_size, d_output))
X = keras.ops.convert_to_tensor(X, dtype="int8")
y = keras.ops.convert_to_tensor(y, dtype="int8")

model.fit(X, y, batch_size=batch_size)

