#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Train a bag2bag neural network model, based on multi-layer perceptron or a global-attention mechanism, for different amount of noise and different predictive sequences.
"""
import os
from itertools import combinations

import tensorflow as tf

import pandas as pd

import iamsequencelearning.sparse2sparse.tensorflow as models
from iamsequencelearning.sequencegenerator.random_sequences import RandomSequences

from iamsequencelearning import tensorflow_train_test as tensorflow


print("TensorFlow version:", tf.__version__)

d_input, d_output = 120, 120
size_train, size_test = 2500, 200
learning_rate = 1e-3
batch_size = 64
epochs = 5

# device = "cuda" if torch.cuda.is_available() else "cpu"  # TODO: replace this

benchmark_sequence = [
    comb for size in range(1, 2)
    for comb in combinations(RandomSequences._sequence_types, size)]
benchmark_model = [m for m in dir(models)
                   if m.startswith('OneHidden') or 'Attention' in m]
benchmark_size = [10, 100, 1000]
benchmark_noise_proportion = [0.5, 0.25, 0.1, 0.01, 0.0]
model_number_total = len(benchmark_sequence) * len(benchmark_size)
model_number_total *= len(benchmark_noise_proportion) * len(benchmark_model)


class AccuraciesCallback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs=None):
        print(logs)
        return epoch, logs


datas = []
model_number = 0
for size in benchmark_size:
    d_input, d_output = size, size
    for noise_proportion in benchmark_noise_proportion:
        for sequence_types in benchmark_sequence:
            dataset_train = RandomSequences(
                d_input=d_input,
                d_output=d_output,
                size=size_train,
                noise_proportion=noise_proportion,
                sequence_types=sequence_types)
            ds_train = tf.data.Dataset.from_tensor_slices(
                dataset_train.datas).shuffle(len(dataset_train)).batch(batch_size)
            dataset_test = RandomSequences(
                d_input=d_input,
                d_output=d_output,
                size=size_test,
                noise_proportion=noise_proportion,
                sequence_types=sequence_types)
            ds_test = tf.data.Dataset.from_tensor_slices(
                dataset_test.datas).shuffle(len(dataset_test)).batch(batch_size)
            for modelname in benchmark_model:
                model_number += 1
                print(f"Training model n°{model_number:>4}/{model_number_total}")
                print(f"{modelname} on sequence {sequence_types} of size {size} with {100*noise_proportion}% noise")
                model = getattr(models, modelname)(
                    d_input=d_input, d_output=d_output, d_hidden=64, dropout=0.1,
                )
                statistics = tensorflow.traintest(
                    epochs=epochs,
                    dataset_train=ds_train,
                    dataset_test=ds_test,
                    model=model,
                    sigmoid='Sigmoid' not in modelname,
                    verbose=True)
                # average on the 10 last epochs
                averaged_statistics = tensorflow.average_statistics(
                    statistics, averaging_size=10)
                datas.append(
                    {'model': modelname,
                     'sequence_size': size,
                     'noise_proportion': noise_proportion,
                     'sequence_types': '_+_'.join(sequence_types),
                     'accuracy_micro_mean': averaged_statistics[0],
                     'accuracy_micro_variance': averaged_statistics[1],
                     'accuracy_macro_mean': averaged_statistics[2],
                     'accuracy_macro_variance': averaged_statistics[3],
                     'accuracy_meso_mean': averaged_statistics[4],
                     'accuracy_meso_variance': averaged_statistics[5],
                     })


foldername = 'data/'
if not os.path.exists(foldername):
    os.makedirs(foldername)

filename = foldername + "test_on_generated_examples_tensorflow.csv"
df = pd.DataFrame(datas)
df.to_csv(filename)
