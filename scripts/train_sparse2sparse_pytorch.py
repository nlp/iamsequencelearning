#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Train a bag2bag neural network model, based on multi-layer perceptron or a global-attention mechanism, for different amount of noise and different predictive sequences.
"""
import os
from itertools import combinations

import torch
import torch.nn as nn

import numpy as np

import pandas as pd

from torch.utils.data import Dataset
from torch.utils.data import DataLoader

from iamsequencelearning.sparse2sparse_pytorch import MultiLayerPerceptron
from iamsequencelearning.sparse2sparse_pytorch import MultiLayerGlobalAttention
from iamsequencelearning.sequencegenerator.random_sequences import RandomSequences

from iamsequencelearning import pytorch_train_test as pytorch

d_input, d_output = 120, 120
size_train, size_test = 2500, 200
learning_rate = 1e-3
batch_size = 64
epochs = 45

device = "cuda" if torch.cuda.is_available() else "cpu"


class RandomSequencesDataset(RandomSequences, Dataset):
    """Random sequence generator of Dataset
    """
    
    def _construct_data(self):
        """Construct the datasets."""
        self.X, self.y = super()._construct_data()
        self.X = torch.Tensor(self.X).to(device)
        self.y = torch.Tensor(self.y).to(device)
        return self.X, self.y


sequences_combinations = [
    comb for size in range(1, 3)
    for comb in combinations(RandomSequences._sequence_types, size)]

datas = []
for size in [10, 100, 1000]:
    print(f"for size={size}")
    d_input, d_output = size, size
    for noise_proportion in [0.5, 0.25, 0.1, 0.01, 0.0]:
        print(f"for noise proportion={noise_proportion}")
        for sequence_types in sequences_combinations:
            print(f"for sequence types {sequence_types}")
            dataset_train = RandomSequencesDataset(
                d_input=d_input,
                d_output=d_output,
                size=size_train,
                noise_proportion=noise_proportion,
                sequence_types=sequence_types)
            dataset_test = RandomSequencesDataset(
                d_input=d_input,
                d_output=d_output,
                size=size_test,
                noise_proportion=noise_proportion,
                sequence_types=sequence_types)
            dataloader_train = DataLoader(dataset_train,
                                          batch_size=batch_size, shuffle=True)
            dataloader_test = DataLoader(dataset_test,
                                         batch_size=batch_size, shuffle=True)

            model = MultiLayerPerceptron(
                d_input=d_input, d_output=d_output, d_hidden=64, dropout=0.1,
            ).to(device)
            loss_function = nn.BCEWithLogitsLoss()
            optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

            print("Model:", model)
            print("Loss function: ", loss_function)
            print("Optimizer: ", optimizer)

            statistics = pytorch.traintest(
                epochs=epochs,
                dataloader_train=dataloader_train,
                dataloader_test=dataloader_test,
                model=model, loss_function=loss_function, optimizer=optimizer)
            averaged_statistics = pytorch.average_statistics(statistics)
            datas.append(
                {'model': 'MultiLayerPerceptron',
                 'sequence_size': size,
                 'noise_proportion': noise_proportion,
                 'sequence_types': '_+_'.join(sequence_types),
                 'accuracy_micro_mean': averaged_statistics[0],
                 'accuracy_micro_variance': averaged_statistics[1],
                 'accuracy_macro_mean': averaged_statistics[2],
                 'accuracy_macro_variance': averaged_statistics[3],
                 'accuracy_meso_mean': averaged_statistics[4],
                 'accuracy_meso_variance': averaged_statistics[5],
                 })

            # MutiLayerGlobalAttention
            model = MultiLayerGlobalAttention(
                d_input=d_input, d_output=d_output, d_head=64, dropout=0.1,
            ).to(device)
            loss_function = nn.BCEWithLogitsLoss()
            optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

            print("Model:", model)
            print("Loss function: ", loss_function)
            print("Optimizer: ", optimizer)

            statistics = pytorch.traintest(
                epochs=epochs,
                dataloader_train=dataloader_train,
                dataloader_test=dataloader_test,
                model=model, loss_function=loss_function, optimizer=optimizer)
            averaged_statistics = pytorch.average_statistics(statistics)
            datas.append(
                {'model': 'GlobalAttention',
                 'sequence_size': size,
                 'noise_proportion': noise_proportion,
                 'sequence_types': '_+_'.join(sequence_types),
                 'accuracy_micro_mean': averaged_statistics[0],
                 'accuracy_micro_variance': averaged_statistics[1],
                 'accuracy_macro_mean': averaged_statistics[2],
                 'accuracy_macro_variance': averaged_statistics[3],
                 'accuracy_meso_mean': averaged_statistics[4],
                 'accuracy_meso_variance': averaged_statistics[5],
                 })

foldername = 'data/'
if not os.path.exists(foldername):
    os.makedirs(foldername)

filename = foldername + "test_on_generated_examples_pytorch.csv"
df = pd.DataFrame(datas)
df.to_csv(filename)

# #%% see one example

# i = int(torch.rand(1).item() * len(dataset_test))
# mla(dataset_test[i][0]).round().type(torch.int)
# dataset_test[i][1].round().type(torch.int)

# #%% calculate the accuracy for all set

# accuracy_train_total = 0
# for X, y_true in dataset_train:
#     y_pred = mla(X)
#     y_pred = y_pred.round().type(torch.int).numpy()
#     y_true = y_true.round().type(torch.int).numpy()
#     accuracy_train_total += (y_true == y_pred).sum() / np.prod(y_true.shape)

# accuracy_train_average = accuracy_train_total / len(dataset_train)
# print(f"Train accuracy: {100*accuracy_train_average:>.3f}%")

# accuracy_test_total = 0
# for X, y_true in dataset_test:
#     y_pred = mla(X)
#     y_pred = y_pred.round().type(torch.int).numpy()
#     y_true = y_true.round().type(torch.int).numpy()
#     accuracy_test_total += (y_true == y_pred).sum() / np.prod(y_true.shape)

# accuracy_test_average = accuracy_test_total / len(dataset_test)
# print(f"Test accuracy: {100*accuracy_test_average:>.3f}%")
