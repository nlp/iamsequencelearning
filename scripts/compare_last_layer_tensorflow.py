#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The problem at hand is a boolean-features to boolean-multilabel prediction task. Both input and output have size 100.

One wants to compare the difference between the training of simple multi-layer network made of dense layers, with the only difference being in the last layer, that can be either a sigmoid.
"""
from typing import Tuple

import tensorflow as tf

from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras import Input, Model

import numpy as np
# random pattern
rng = np.random.default_rng()

example_size = 100
example_number = 1000
example_test = 250

batch_size = 24


def random():
    """Completely random sequences."""
    X = rng.integers(0, 2, size=(example_number, example_size))
    y = rng.integers(0, 2, size=(example_number, example_size))
    return X, y


def invert():
    """If input is 0, output is 1, and vice-versa."""
    X = rng.integers(0, 2, size=(example_number, example_size))
    return X, 1 - X


def Z2():
    """Alternate series of 0 and 1, only the starting position of X is random. The starting position of y is the same as y."""
    # generate the seeds
    X = np.array([list(rng.permutation([0, 1])) * (example_size // 2)
                  for _ in range(example_number)])
    return X, X

def predict(model, x, sigmoid: bool = False):
    """Predict the sequence associated to the sequence x via the model."""
    y = model.predict(x)
    if bool(sigmoid):
        y = tf.math.sigmoid(y)  # in case the last layer has no sigmoid activation
    return tf.round(y).numpy().astype(int)


def accuracies(y_pred, y_true):
    """Compare y_true and y_pred on microscopic and macroscopic accuracies."""
    accuracy_micro = (y_true == y_pred).sum() / np.prod(y_true.shape)
    accuracy_macro = np.all(y_true == y_pred, axis=1).sum() / y_true.shape[0]
    return accuracy_micro, accuracy_macro

X1, y1 = random()
X2, y2 = Z2()
train_dataset = np.vstack([X1, X2]), np.vstack([y1, y2])
train_dataset = tf.data.Dataset.from_tensor_slices(
    train_dataset).shuffle(2*example_number).batch(batch_size)

model_sigmoid = OneHiddenSigmoid()
model_sigmoid.fit(train_dataset, epochs=50)
