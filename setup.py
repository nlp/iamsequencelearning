#!/usr/bin/env python

from logging import error
import setuptools

try:
    with open("README.md", "r") as file:
        long_description_ = file.read()
except FileNotFoundError:
    long_description_ = "long description not found: no README.md file"
    error(long_description_)

if __name__ == "__main__":
    setuptools.setup(
        packages=setuptools.find_packages(),
        include_package_data=True,
        long_description=long_description_,
        long_description_content_type="text/markdown",
        )
