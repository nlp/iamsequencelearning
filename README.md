# SequenceLearning

A sequence is an ordered array of elements, each element being taken from an alphabet (or thesaurus, or ..., i.e. a finite collection of symbols).

## Different types of tasks

Everytime the alphabet is known and finite. A symbol is an element of the alphabet. One wants to predict one or several symbols $y$ given one or several symbols $x$. Depending on the cardinality

 - masked [language models](https://en.wikipedia.org/wiki/Language_model) : given $n$-symbols, their positions $p_X$ and some positions $p_y$, predict the elements at position $p_y$'s. There are several sub-examples of such task, as e.g.
   - [vanilla language model]() : given $n$ previous symbol, predict the next symbol
   - [ngrams language model]() : given $n$ symbols surrounding the position $p_y$, predict the symbol $y$
   - [skipgram language model]() : given a position $p$ and its symbol, predict the $m$ surrounding symbols
   - [autoregressive models](https://en.wikipedia.org/wiki/Autoregressive_model) : given the $n$-previous symbols, predict the next one (_in terms of symbols this is the vanilla language model_)
 - global ordering : given $n$-symbols, predict their order
    - 2-by-2 ordering : given two symbols, predict their order / this is a subtask of the ordering task

## Multi-alphabet tasks

In some cases, one has in fact several datas that are ordered and correlated. When these datas are taken in some alphabets, one has the situation when there are several sequences progressing together. One may think of completing some interrupted sequence by means of the known one, or reinforcing the learning of two sequences progressing in parallel but shifted in time (one sequence is known on odd time steps, while the other is known on even time steps)
